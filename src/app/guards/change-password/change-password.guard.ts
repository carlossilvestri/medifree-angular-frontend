import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SecurityQService } from 'src/app/services/security-q/security-q.service';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordGuard implements CanActivate {

  constructor(
    public _securityQService: SecurityQService,
    public router: Router
  ) { }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      // For debugging.
      console.log('En change password guard.  this._securityQService.canChangePassword ', this._securityQService.canChangePassword);
      if(this._securityQService.canChangePassword){
        return true;
      }else{
        this.router.navigateByUrl('/login');
        return false;
      }
  }
  
}
