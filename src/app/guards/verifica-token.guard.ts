import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class VerificaTokenGuard implements CanActivate {

  constructor(
    public _usuarioService: UserService,
    public router: Router
  ) { }

  canActivate(): Promise<boolean> | boolean {

    console.log('Token guard');

    let token : string = this._usuarioService.cargarStorage('token');
    let payload = JSON.parse( atob( token.split('.')[1] ));
    let expirado = this.expirado( payload.exp );
    /*
    // For debugging.
    console.log('payload ', payload);
    console.log('expirado ', expirado);
    console.log('this.verificaRenueva( payload.exp ) ', this.verificaRenueva( payload.exp ));
    */
    if ( expirado ) {
      // Renovar el token.
      this._usuarioService.getRefreshedToken();
      // this.router.navigate(['/login']);
      return false;
    }


    return this.verificaRenueva( payload.exp );
  }


  verificaRenueva( fechaExp: number ): Promise<boolean>  {

    return new Promise( (resolve, reject) => {

      let tokenExp = new Date( fechaExp * 1000 );
      let ahora = new Date();

      ahora.setTime( ahora.getTime() + ( 1 * 60 * 60 * 1000 ) );

      // console.log( tokenExp );
      // console.log( ahora );

      if ( tokenExp.getTime() > ahora.getTime() ) {
        resolve(true);
      } else {

        this._usuarioService.getRefreshedTokenCallHttp()
              .subscribe( () => {
                resolve(true);
              }, () => {
                this.router.navigate(['/login']);
                reject(false);
              });

      }

    });

  }


  expirado( fechaExp: number ) {

    let ahora = new Date().getTime() / 1000;

    if ( fechaExp < ahora ) {
      return true;
    }else {
      return false;
    }


  }



}
