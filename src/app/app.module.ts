// Modulos
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgImageSliderModule } from 'ng-image-slider';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DatePipe } from '@angular/common';
// Componentes
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { CrearCuentaComponent } from './pages/crear-cuenta/crear-cuenta.component';
import { BarranavegacionComponent } from './components/barranavegacion/barranavegacion.component';
import { HomeComponent } from './pages/home/home.component';
import { OlvidarpasswordComponent } from './pages/olvidarpassword/olvidarpassword.component';
import { SecurityQuestionsComponent } from './pages/security-questions/security-questions.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { BarraArribaComponent } from './components/barra-arriba/barra-arriba.component';
import { MedicamentoMuestrasComponent } from './components/medicamento-muestras/medicamento-muestras.component';
import { RegisterMedicineComponent } from './components/register-medicine/register-medicine.component';
import { SolicitudDonacionComponent } from './components/solicitud-donacion/solicitud-donacion.component';
import { DetalleMedicamentoComponent } from './components/detalle-medicamento/detalle-medicamento.component';
import { DetalleSolicitudDonacionComponent } from './components/detalle-solicitud-donacion/detalle-solicitud-donacion.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { EditSecurityQComponent } from './components/edit-security-q/edit-security-q.component';
import { MotivoDonacionComponent } from './components/motivo-donacion/motivo-donacion.component';
import { LoadingComponent } from './components/loading/loading.component';
import { InterceptorErrorInterceptor } from './interceptors/interceptor-error/interceptor-error.interceptor';
import { UserService } from './services/user.service';
import { DonantesSeleccionadosComponent } from './components/donantes-seleccionados/donantes-seleccionados/donantes-seleccionados.component';
import { VerificaTokenGuard } from './guards/verifica-token.guard';
import { FlechaRegresarComponent } from './components/flecha-regresar/flecha-regresar.component';
import { ChangePasswordGuard } from './guards/change-password/change-password.guard';
import { PaginationComponent } from './components/pagination/pagination.component';
import { AdministracionComponent } from './pages/administracion/administracion.component';
import { UsuariosComponent } from './pages/administracion/usuarios/usuarios.component';
import { MedicamentosComponent } from './pages/administracion/medicamentos/medicamentos.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CrearCuentaComponent,
    BarranavegacionComponent,
    HomeComponent,
    OlvidarpasswordComponent,
    SecurityQuestionsComponent,
    ChangePasswordComponent,
    BarraArribaComponent,
    MedicamentoMuestrasComponent,
    RegisterMedicineComponent,
    SolicitudDonacionComponent,
    DetalleMedicamentoComponent,
    DetalleSolicitudDonacionComponent,
    EditUserComponent,
    EditSecurityQComponent,
    MotivoDonacionComponent,
    LoadingComponent,
    DonantesSeleccionadosComponent,
    FlechaRegresarComponent,
    PaginationComponent,
    AdministracionComponent,
    UsuariosComponent,
    MedicamentosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgImageSliderModule
  ],
  providers: [
    VerificaTokenGuard,
    ChangePasswordGuard,
    DatePipe, 
  {
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptorErrorInterceptor,
    multi: true,
    deps: [UserService, AppRoutingModule]
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
