import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {
  constructor() {
   }
  toggleSlideMenu(): void {
    // Como esta parte es JavaScript puro, se debe realizar un peq retrazo al cargar la funcion ya que angular aun no carga los elemento.
    setTimeout(() => {
      document.getElementById('menu').classList.toggle('barra-active');
      let arrayImgs = document.getElementsByClassName('img-efecto');
      // console.log('arrayimg ', arrayImgs);
      for (let i = 0; i < arrayImgs.length; i++) {
        arrayImgs[i].classList.toggle('animate__backOutLeft');
      }
      let arrayElemtsEfect2 = document.getElementsByClassName('img-efecto2');
      for (let i = 0; i < arrayElemtsEfect2.length; i++) {
        arrayElemtsEfect2[i].classList.toggle('animate__fadeIn');
      }
      document.getElementById('burger-btn').classList.toggle('d-none');
    }, 300);
  }
  closeSlideMenu(): void {
    // Como esta parte es JavaScript puro, se debe realizar un peq retrazo al cargar la funcion ya que angular aun no carga los elemento.
    setTimeout(() => {
      document.getElementById('menu').classList.remove('barra-active');
      document.getElementById('burger-btn').classList.add('d-none');
    }, 400);
  }
  openSlideMenu(): void {
    // Como esta parte es JavaScript puro, se debe realizar un peq retrazo al cargar la funcion ya que angular aun no carga los elemento.
    setTimeout(() => {
      document.getElementById('menu').classList.add('barra-active');
      document.getElementById('burger-btn').classList.add('d-none');

    }, 400);
  }
}
