import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { PeticionDonacion, PeticionDonacionDatosReqCrear, PeticionDonacionGetResponse } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class PeticionDonacionService {
  // Variables
  private url = environment.baseUrl;
  peticionDonacion: PeticionDonacion[];
  loadingPeticionDonacion: boolean = false;
  huboUnError: boolean = false;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }


        /*
  Doc: createPeticionDonacion. Hace una pet post a la BD para crear una peticion de donacion
  Param: obj: (peticionDonacionDatosReq: PeticionDonacionDatosReqCrear
  */
    createPeticionDonacion(peticionDonacionDatosReq: PeticionDonacionDatosReqCrear) {
      // Los mails funcionan en heroku, en el servidor de hosting maracaibo no esta funcionando.
      const url = `https://backend-medifree.herokuapp.com/peticion-donacion`;
      return this.http.post(url, peticionDonacionDatosReq)
      .pipe(
        catchError( this.userService.handleError),
        map((resp: PeticionDonacionGetResponse | any) => {
          console.log(resp);
          if(resp.ok){
            Swal.fire('Correcto', 'La peticion de la donación fue creada con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
  /*
  Doc: getPeticionesDonacionDelDonador. Hace una pet a la BD para obtener las medicinas disponibles, de un usuario registrado en particular por su id, paginados.
  Return: obj: PeticionDonacionGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getPeticionesDonacionDelDonador(token : string, page : number = 0) : PeticionDonacionGetResponse | any {
    const url = `${this.url}/peticion-donacion-donador?token=${token}&desde=${page}`;
    this.loadingPeticionDonacion = true;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: PeticionDonacionGetResponse | any) => {
        console.log(resp);
        this.updatePeticionDonacionLocalStorage(resp);
        this.huboUnError = false;
        this.peticionDonacion = resp.peticionDonacion;
        this.loadingPeticionDonacion = false;
        return resp;
      })
    );
  }

   /*
  Doc: getPeticionesDonacionDelSolicitante. Hace una pet a la BD para obtener las medicinas disponibles, de un usuario registrado en particular por su id, paginados.
  Return: obj: PeticionDonacionGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getPeticionesDonacionDelSolicitante(token : string, page : number = 0) : PeticionDonacionGetResponse | any {
    const url = `${this.url}/peticion-donacion-solicitante?token=${token}&desde=${page}`;
    this.loadingPeticionDonacion = true;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: PeticionDonacionGetResponse | any) => {
        console.log(resp);
        this.updatePeticionDonacionLocalStorage(resp);
        this.huboUnError = false;
        this.peticionDonacion = resp.peticionDonacion;
        this.loadingPeticionDonacion = false;
        return resp;
      })
    );
  }


    updatePeticionDonacionLocalStorage(peticionDonacionDatosReq: PeticionDonacionGetResponse): PeticionDonacionGetResponse | any {
      return this.userService.guardarStorage('peticion-donacion', peticionDonacionDatosReq);
    }
    getPeticionDonacionLocalStorage(): PeticionDonacionGetResponse | any {
      return this.userService.cargarStorage('peticion-donacion');
    }
    borrarPeticionDonacionLocalStorage(): void {
      this.userService.eliminarCampoLocalStorage('peticion-donacion');
    }
}
