import { TestBed } from '@angular/core/testing';

import { PeticionDonacionService } from './peticion-donacion.service';

describe('PeticionDonacionService', () => {
  let service: PeticionDonacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeticionDonacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
