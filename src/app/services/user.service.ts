import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {
  EditUserNoImage,
  UserAlCrear,
  LoginDatosRequeridos,
  LoginResponse,
  EditUserByIdResponse,
  User,
  GetDatosAdm,
  GetUser,
  ModificarPasswordResponse
} from '../interfaces/interfaces';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, pipe, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // Variables
  public url = environment.baseUrl;
  token: string = '';
  usuario: User; 
  constructor(public http: HttpClient, public router: Router) {}

  averiguarUrl(): void {
    console.log('this.url ', this.url);
  }
  /*
  Doc: Crea un usuario nuevo en la BD.
  Param: obj: UserAlCrear
  */
  crearUsuario(usuario: UserAlCrear) {
    const url = `${this.url}/user`;
    return this.http.post(url, usuario).pipe(
      // catchError(this.handleError),
      map((resp: any) => {
        Swal.fire('Correcto', 'Usuario creado ' + usuario.emailU, 'success');
        return resp.usuario;
      })
    );
  }
  /*
  Doc: Login. Hace una pet a la BD para comprobar que los datos sean correctos.
  Param: obj: LoginDatosRequeridos
  */
  login(datoLogin: LoginDatosRequeridos) {
    const url = `${this.url}/login`;
    return this.http.post(url, datoLogin).pipe(
      // catchError(this.handleError),
      map((resp: LoginResponse) => {
        console.log(resp);
        this.usuario = resp.user;
        this.guardarStorage('user', resp.user);
        this.guardarStorage('token', resp.token);
        this.token = resp.token;
        return resp;
      })
    );
  }
  /*
  Doc: logout. Elimina el campo user del localstorage e inmedeitamente envia al usuario a la pag del login.
  */
  logout() {
    this.eliminarCampoLocalStorage('user');
    this.eliminarCampoLocalStorage('token');
    this.eliminarCampoLocalStorage('security-questions');
    this.eliminarCampoLocalStorage('categories');
    this.eliminarCampoLocalStorage('medicines-by-city');
    this.eliminarCampoLocalStorage('medicines-all-cities');
    this.eliminarCampoLocalStorage('city-selected');
    this.eliminarCampoLocalStorage('medicines-by-category-id');
    this.eliminarCampoLocalStorage('medicines-of-user');
    this.eliminarCampoLocalStorage('medicines-seen-present');
    this.eliminarCampoLocalStorage('donantes-s-creador');
    this.router.navigate(['/login']);
  }
  /*
  Doc: editUser. Hace una pet a la BD para editar los datos del usuario.
  Param: obj: LoginDatosRequeridos
  */
  editUser(user: EditUserNoImage) {
    const url = `${this.url}/user/${user.idUser}`;
    return this.http.put(url, user).pipe(
      // catchError(this.handleError),
      map((resp: EditUserByIdResponse | any) => {
        console.log(resp);
        if (resp.ok) {
          this.guardarStorage('user', resp.user);
          Swal.fire(
            'Correcto',
            'Usuario editado ' + resp.user.namesU + ' con éxito.',
            'success'
          );
        }
        return resp;
      })
    );
  }
    /*
  Doc: editUser. Hace una pet a la BD para editar los datos del usuario.
  Param: obj: LoginDatosRequeridos
  */
  changeOnlyPassword(token: string, password: string, password2: string,) {
    const url = `${this.url}/user/modify-password/password`;
    const obj = {
      token,
      password,
      password2
    }
    return this.http.put(url, obj).pipe(
      // catchError(this.handleError),
      map((resp: EditUserByIdResponse | any) => {
        console.log(resp);
        if (resp.ok) {
          Swal.fire(
            'Correcto',
            'La constraseña fue cambiada con éxito.',
            'success'
          );
        }
        return resp;
      })
    );
  }
  /*
  Doc: guardarStorage. Guarda cualquier campo con su respectivo obj en el localstorage.
  Param: campo: string, objeto: any
  */
  guardarStorage(campo: string, objeto: any): void {
    const objEnString: string = JSON.stringify(objeto);
    localStorage.setItem(campo, objEnString);
  }
  /*
  Doc: eliminarCampoLocalStorage. Eliminar cualquier campo del localstorage.
  Param: campoAEliminar: string
  */
  eliminarCampoLocalStorage(campoAEliminar: string): void {
    localStorage.removeItem(campoAEliminar);
  }
  /*
  Doc: cargarStorage. Carga en una variable cualquier campo del localstorage.
  Param: campoASolicitar: string
  */
  cargarStorage(campoASolicitar: string) {
    var campo: any;
    if (localStorage.getItem(campoASolicitar)) {
      campo = JSON.parse(localStorage.getItem(campoASolicitar));
    } else {
      campo = null;
    }
    return campo;
  }

  /* Handle Errors */
  public handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error) {
      if (error.error.msg) {
        if (
          error.error.msg ===
          'No se encontraron preguntas de seguridad según el email especificado'
        )
          return;
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error.msg,
        });
      }
      // Ver si se vencio el token.
      if (error.error.errors.message === 'jwt expired') {
        console.log('err ', error.error.errors.message);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Se vencio el token.',
        });
      }
    }
    if (error.status == 0) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Compruebe la conexión a Internet.',
      });
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }
  /* Token */
  /*
  Doc: getRefreshedToken. Hace una pet a la BD para obtener las solicitudes de donacion del solicitante, paginadas.
  Return: obj: PeticionDonacionGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getRefreshedTokenCallHttp(): any {
    const user: User = this.cargarStorage('user');
    const token: string = this.cargarStorage('token');
    const url = `${this.url}/refresh-token?idUser=${user.idUser}&token=${token}`;
    return this.http.get(url).pipe(
      // catchError(this.handleError),
      map((resp: any) => {
        console.log(resp);
        console.log('Token renovado');
        this.token = resp.token;
        this.guardarStorage('token', resp.token);
        return resp;
      })
    );
  }
  getRefreshedToken() {
    // Actualizar storage de la ciudad seleccionada.
    this.getRefreshedTokenCallHttp().subscribe((resp) => {
      console.log('resp getRefreshedToken ', resp);
    });
  }
  /** -- ADMINISTRADOR -- **/
  /**
   obtenerDatosDeAdministracion() : Observable
   Obtiene los datos que muestran las cantidades que hay de usuarios, medicamentos, categorias, paises, estados, ciudades.
  */
  obtenerDatosDeAdministracion(){
    const url = `${this.url}/adm-statistics`;
        return this.http.get(url).pipe(
          map((resp: GetDatosAdm) => {
            return resp;
          })
        );
  }
  /*
  Doc: getUsersAllCountries. Hace una pet a la BD para obtener los usuarios disponibles, de todos los paises, paginados.
  Return: 
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getUsersAllCountries(desde : number = 0)  {
    const url = `${this.url}/user?desde=${desde}`;
    return this.http.get(url)
    .pipe(
      map((resp: GetUser) => {
        return resp;
      })
    );
  }
  /**
  */
  /*
  Doc: getUsersAllCountries. Hace una pet a la BD para obtener los usuarios disponibles, de todos los paises, paginados segun el nombre.
  Return: 
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getUsersAllCountriesByKeyword(namesU: string, desde : number = 0)  {
    const url = `${this.url}/users-by-keyword-all-countries?namesU=${namesU}&desde=${desde}`;
    return this.http.get(url)
    .pipe(
      map((resp: GetUser) => {
        return resp;
      })
    );
  }
  /*
  Doc: habilitarODeshabilitarUsuario.
  Habilita o deshabilita un usuario de la BD
  Return: 
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  habilitarODeshabilitarUsuario(id : number, isActive: boolean)  {
    const url = `${this.url}/activate-user/${id}`;
    let obj = {
      isActive
    }
    return this.http.patch(url, obj)
    .pipe(
      map((resp: ModificarPasswordResponse) => {
        return resp;
      })
    );
  }
    /*
  Doc: habilitarODeshabilitarUsuario.
  Habilita o deshabilita un usuario de la BD
  Return: 
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  permisosDeAdm(id : number, isSuperAdministrator: boolean)  {
    const url = `${this.url}/super-adm/${id}`;
    let obj = {
      isSuperAdministrator
    }
    return this.http.patch(url, obj)
    .pipe(
      map((resp: ModificarPasswordResponse) => {
        return resp;
      })
    );
  }
}
