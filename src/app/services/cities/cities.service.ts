import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CiudadesGetResponse } from '../../interfaces/interfaces';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  private url = environment.baseUrl;
  public selectedCity = '';
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

  /*
  Doc: getCitiesCallHttp. Hace una pet a la BD para obtener un array de ciudades.
  Return: obj: CiudadesGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getCitiesCallHttp() : CiudadesGetResponse | any {
    const url = `${this.url}/ciudad`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: CiudadesGetResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('cities', resp);
        return resp;
      })
    );
  }
    /*
  Doc: getCitiesCallHttp. Hace una pet a la BD para obtener un array de ciudades.
  Return: obj: CiudadesGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getCitiesByStateIdCallHttp(idEstado: number) : CiudadesGetResponse | any {
    const url = `${this.url}/ciudad-por-idestadof?idEstadoF=${idEstado}`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: CiudadesGetResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('cities', resp);
        return resp;
      })
    );
  }
  getCitiesLocalStorage(): CiudadesGetResponse | any {
    return this.userService.cargarStorage('cities');
  }
  borrarCitiesLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('cities');
  }
  /*
  Funcion que guarda en local storage el estado seleccionado y actualiza el valor de la variable selectedState.
  */
  saveCitySelected(selectedCity: string) : void{
    this.selectedCity = selectedCity;
    this.userService.guardarStorage(
      'city-selected',
      this.selectedCity
    );
  }


}
