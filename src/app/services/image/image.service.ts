import { HttpClient, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UserService } from '../user.service';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ImgGetResponse, Imagen, Imagen2 } from '../../interfaces/interfaces';
@Injectable({
  providedIn: 'root'
})
export class ImageService {
  public url = environment.baseUrl;
  public images: Imagen2[] = [];
  public loadingImages: boolean = false;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }
     /*
  Doc: getStatesByPaisIdCallHttp. Hace una pet a la BD para obtener un array de ciudades.
  Return: obj: EstadoGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getImagesCallHttp(tipo: string, id: number) : Observable<ImgGetResponse> {
    console.log("En getImagesCallHttp");
    const url = `${this.url}/image/${tipo}/${id}`;
    this.loadingImages = true;
    this.images = [];
    return this.http.get(url)
    .pipe(
      map((resp: ImgGetResponse) => {
        console.log(resp);
        resp.imagenes.forEach(element => {
          let rutaImagen =  `${this.url}/upload/${tipo}/${element.nameImage}`;
          let objImagen = {
            image: rutaImagen,
            thumbImage: rutaImagen,
            main: element.mainImage,
            id: element.idImage,
            order: (element.mainImage) ? 1 : 0
          }
          this.images.push(objImagen);
        });
        if(this.images.length === 0){
          let noImage = `${this.url}/upload/${tipo}/no-image`;
          let objImagenVacio = {
            image: noImage,
            thumbImage: noImage,
            main: false,
            id: 0,
            order: 1 
          }
          this.images.push(objImagenVacio);
        }
        this.images.sort(
          (a, b) => {
            if (a.main.toString() > b.main.toString()) {
              return -1;
            }
            if (a.main.toString() < b.main.toString()) {
              return 1;
            }
            return 0;
          }
        );
        this.loadingImages = false;
        return resp;
      })
    );
  }
                  /*
  Doc: deleteDonanteS. Hace una pet delete a la BD para borrar un medicamento de un usuario.
  Param: obj: (medicine: MedicineDatosEditRequeridos
  */
  deleteAnImage(idMedicamento: number) {
      const url = `${this.url}/image/${idMedicamento}`;
      return this.http.delete(url)
      .pipe(
        // catchError( this.handleError),
        map((resp: any) => {
          console.log(resp);
          if(resp.ok){
            let posicionABorrarDelArray: number = 0;
            // Conocer la posicion del medicamento borrado.
            for (let i: number = 0; i < this.images.length; i++) {
              if (this.images[i].id === idMedicamento) {
                posicionABorrarDelArray = i;
                console.log('posicionABorrarDelArray! ', posicionABorrarDelArray);
                // Eliminar del array de donanteSeleccionados el medicamento borrado.
                this.images.splice(posicionABorrarDelArray, 1);
                return;
              }
            }
          }
          return resp;
        })
      );
  
    }
    editMainImgField(idImagen: number, mainImage : boolean){
      const url = `${this.url}/image-edit/${idImagen}`;
      const obj = {
        mainImage 
      } 
      console.log("obj ", obj)
      return this.http.patch(url, obj)
      .pipe(
        // catchError( this.handleError),
        map((resp: any) => {
          console.log(resp);
          if(resp.ok){
            let posicionABorrarDelArray: number = 0;
            /*
            // Conocer la posicion del medicamento borrado.
            for (let i: number = 0; i < this.images.length; i++) {
              if (this.images[i].id === idImagen) {
                posicionABorrarDelArray = i;
                console.log('posicionABorrarDelArray! ', posicionABorrarDelArray);
                // Eliminar del array de donanteSeleccionados el medicamento borrado.
                this.images[i].main = mainImage;
              }
            }
            */
          }
          return resp;
        })
      );
    }
    addImg(tipo: string, id: number, imagen: File, token: string){
      const url = `${this.url}/image/${tipo}/${id}`;
      const fd = new FormData();
      fd.append('imagen', imagen);
      fd.append('tipo', tipo);
      fd.append('mainImage', "true");
      fd.append('token', token);
      return this.http.post(url, fd)
      .pipe(
        map((resp: any) => {
          console.log(resp);
          return resp;
        })
      );
    }
    editImg(idImg: number, imagen: File){
      console.log("Desde image.service editImg", imagen);
      const url = `${this.url}/image-edit/${idImg}`;
      const fd = new FormData();
      fd.append('imagen', imagen);
      fd.append('mainImage', "true");
      return this.http.post(url, fd)
      .pipe(
        map((resp: any) => {
          console.log(resp);
          return resp;
        })
      );
    }
}
