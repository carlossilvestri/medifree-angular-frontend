import { TestBed } from '@angular/core/testing';

import { CategoryMedService } from './category-med.service';

describe('CategoryMedService', () => {
  let service: CategoryMedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CategoryMedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
