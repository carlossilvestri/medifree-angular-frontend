import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { CategoryResponse, Categoria } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryMedService {
  private url = environment.baseUrl;
  categoriasResponse: CategoryResponse;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

    /*
  Doc: getCitiesCallHttp. Hace una pet a la BD para obtener un array de ciudades.
  Return: obj: CiudadesGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getCategoriesCallHttp() : CategoryResponse | any {
    const url = `${this.url}/categoria`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: CategoryResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('categories', resp.categorias);
        this.categoriasResponse = resp;
        return resp;
      })
    );
  }
  /*
  Doc: editCategoryyId. Hace una pet a la BD para editar los datos de las categorias de medicamentos.
  Param: obj: (category: CategoryResponse
  */
    editCategoryyId(category: Categoria): CategoryResponse | any {
      const url = `${this.url}/category/${category.idCategoria}`;
      return this.http.put(url, category)
      .pipe(
        catchError( this.userService.handleError),
        map((resp: CategoryResponse | any) => {
          console.log(resp);
          if(resp.ok){
            
            // Actualizar del array de categories el campo actualizado. (Fijarse por el id)
            this.editarCategoriaEspecificaDelArray(resp.categoria);
            
            Swal.fire('Correcto', 'La Categoría fue editada con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
    /*
  Doc: deleteCategoryyId. Hace una pet a la BD para borrar los datos de las categorias de los medicamentos.
  Param: obj: (category: CategoryResponse
  */
    deleteCategoryyId(category: Categoria): CategoryResponse | any {
      const url = `${this.url}/category/${category.idCategoria}`;
      return this.http.delete(url)
      .pipe(
        catchError( this.userService.handleError),
        map((resp: CategoryResponse | any) => {
          console.log(resp);
          if(resp.ok){
            /*
            // Actualizar del array de categories el campo actualizado. (Fijarse por el id)
            this.userService.guardarStorage('categories', resp.categoria);
            */
            Swal.fire('Correcto', 'La Categoría fue borrada con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
  editarCategoriaEspecificaDelArray(category: Categoria){
    let categorias: CategoryResponse = this.getCategoriesLocalStorage();
    categorias.categorias.forEach(categoryElement => {
        if(categoryElement.idCategoria == category.idCategoria){
          categoryElement =  category;
        }
    });
    this.categoriasResponse = categorias;
    console.log('categorias ', categorias);
    /*
    this.userService.guardarStorage('categories', categorias);
    */
  }
  getCategoriesLocalStorage(): CategoryResponse | any {
    return this.userService.cargarStorage('categories');
  }
  borrarCategoriesLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('categories');
  }
  iniciarlizarObjCategoriasSinDatos(): Categoria[] {
    return [
      {
        idCategoria:   0,
        nameCategoria: 'No hay categorías',
        isVisible:     true,
        createdAt:     new Date(),
        updatedAt:     new Date(),
      }
    ]
  }
}
