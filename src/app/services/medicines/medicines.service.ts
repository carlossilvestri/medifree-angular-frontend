import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Medicine, MedicineDatosEditRequeridos, MedicineDatosRequeridos, MedicineGetPublicResponse, MedicineLong, MedicineResponse } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class MedicinesService {
  // Variables
  private url = environment.baseUrl;
  public medicines: MedicineLong[] = [];
  public loadingMedicines: boolean = false;
  public editMedicine: boolean = false; // Variable que debe consultar el componente RegisterMedicine para que no se confunda con agregar.
  huboUnError: boolean = false;
  public page: number = 0;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

      /*
  Doc: createMedicine. Hace una pet post a la BD para crear los datos de los medicamentos (Excepto la foto).
  Param: obj: (medicine: MedicineDatosRequeridos
  */
    createMedicine(medicine: MedicineDatosRequeridos) {
      const url = `${this.url}/medicine`;
      return this.http.post(url, medicine)
      .pipe(
        // catchError( this.handleError),
        map((resp: MedicineResponse | any) => {
          console.log(resp);
          if(resp.ok){
            // Editar el medicamento del local storage, cosas por hacera.
            this.medicines.unshift(resp.medicine); // Actualizar momentaneamente el array para que se muestre el nuevo medicamento creado en el home de 1ero.
            let medicinesSeenSelected : MedicineGetPublicResponse = this.getMedicinesSeenPresentLocalStorage();
            medicinesSeenSelected.medicines = this.medicines;
            this.updateMedicinesSeenPresentLocalStorage(medicinesSeenSelected);
            Swal.fire('Correcto', 'El medicamento fue creado con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
          /*
  Doc: editMedicineOfUser. Hace una pet put a la BD para editar los datos de los medicamentos (Excepto la foto).
  Param: obj: (medicine: MedicineDatosEditRequeridos
  */
    editMedicineOfUser(medicine: MedicineDatosEditRequeridos) {
      const url = `${this.url}/medicine/${medicine.idMedicine}`;
      return this.http.put(url, medicine)
      .pipe(
        // catchError( this.handleError),
        map((resp: MedicineResponse | any) => {
          console.log(resp);
          if(resp.ok){
            // Editar el medicamento del local storage, cosas por hacera.
            Swal.fire('Correcto', 'El medicamento fue editado con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
              /*
  Doc: deleteMedicineOfUser. Hace una pet delete a la BD para borrar un medicamento de un usuario.
  Param: obj: (medicine: MedicineDatosEditRequeridos
  */
    deleteMedicineOfUser(idMedicine: number, token: string) {
      const url = `${this.url}/medicine/${idMedicine}?token=${token}`;
      return this.http.delete(url)
      .pipe(
        // catchError( this.handleError),
        map((resp: MedicineResponse | any) => {
          console.log(resp);
          if(resp.ok){
            let positionMedicamentoABorrarDelArray: number = 0;
            // Conocer la posicion del medicamento borrado.
            for (let i: number = 0; i < this.medicines.length; i++) {
              if (this.medicines[i].idMedicine === idMedicine) {
                positionMedicamentoABorrarDelArray = i;
                console.log('positionMedicamentoABorrarDelArray! ', positionMedicamentoABorrarDelArray);
                // Eliminar del array de medicines el medicamento borrado.
                this.medicines.splice(positionMedicamentoABorrarDelArray, 1);
                return;
              }
            }
            // Notificacion de que el medicamento se borro con exito.
            Swal.fire('Correcto', 'El medicamento fue borrado con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
  /*
  Doc: updateIsActivMedicineOfUser. Hace una pet delete a la BD para deshabilitar un medicamento de un usuario.
  Param: obj: (medicine: MedicineDatosEditRequeridos
  */
    updateIsActivMedicineOfUser(idMedicine: number, token: string, isActive: boolean = false) {
      const obj = {
        isActive,
        token
      }
      const url = `${this.url}/medicine-is-active/${idMedicine}`;
      return this.http.put(url, obj)
      .pipe(
        // catchError( this.handleError),
        map((resp: MedicineResponse | any) => {
          console.log(resp);
          if(resp.ok){
            let positionMedicamentoABorrarDelArray: number = 0;
            // Conocer la posicion del medicamento borrado.
            for (let i: number = 0; i < this.medicines.length; i++) {
              if (this.medicines[i].idMedicine === idMedicine) {
                positionMedicamentoABorrarDelArray = i;
                console.log('positionMedicamentoABorrarDelArray! ', positionMedicamentoABorrarDelArray);
                // Eliminar del array de medicines el medicamento borrado.
                this.medicines.splice(positionMedicamentoABorrarDelArray, 1);
                return;
              }
            }
            // Notificacion de que el medicamento se borro con exito.
            Swal.fire('Correcto', 'El medicamento fue deshabilitado con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }

  /*
  Doc: getMedicinesAllCities. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
 /*
  getMedicinesAllCities(page : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine?desde=${page}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('medicines-all-cities', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  */
  /*
  Doc: getMedicinesAllCities. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */

  getMedicinesAllStates(idPais: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-country?desde=${desde}&idPais=${idPais}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('medicines-all-states', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }

    /*
  Doc: getMedicinesByCity. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByCity(idCiudad : number = 0, page : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-city?desde=${page}&idCiudad=${idCiudad}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-by-city', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
    /*
  Doc: getMedicinesByState. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByState(idEstado : number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-state?desde=${desde}&idEstado=${idEstado}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('medicines-by-state', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
    /*
  Doc: getMedicinesByCountry. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByCountry(idPais : number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-country?desde=${desde}&idPais=${idPais}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-by-country', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
    /*
  Doc: getMedicinesAllCountries. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesAllCountries(desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine?desde=${desde}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-by-country', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  /*
  Doc: getMedicinesOfUserId. Hace una pet a la BD para obtener las medicinas disponibles, de un usuario registrado en particular por su id, paginados.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesOfUserId(token : string, page : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-user-id?desde=${page}&token=${token}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('medicines-of-user', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
        /*
  Doc: getMedicinesByCategoryIdAllCountries. Hace una pet a la BD para obtener las medicinas disponibles de una categoria en particular.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByCategoryIdAllCountries(idCategoria : number, page : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-category-id?desde=${page}&idCategoria=${idCategoria}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-by-category-id', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  /*
  Doc: getMedicinesByCategoryIdAndByCountry. Hace una pet a la BD para obtener las medicinas disponibles de una categoria en particular y un pais especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByCategoryIdAndByCountry(idCategoria : number, idCountry: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-category-id-and-country-id?desde=${desde}&idCategoria=${idCategoria}&idPaisF=${idCountry}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-by-category-id', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  /*
  Doc: getMedicinesByCategoryIdAndByState. Hace una pet a la BD para obtener las medicinas disponibles de una categoria en particular y un estado especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByCategoryIdAndByState(idCategoria : number, idEstado: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-state-and-category?desde=${desde}&idCategoriaF=${idCategoria}&idEstadoF=${idEstado}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-by-category-id', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
   /*
  Doc: getMedicinesByCategoryIdAndByCity. Hace una pet a la BD para obtener las medicinas disponibles de una categoria en particular y un estado especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByCategoryIdAndByCity(idCategoria : number, idCiudad: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-city-and-category?desde=${desde}&idCategoriaF=${idCategoria}&idCiudad=${idCiudad}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('medicines-by-category-id', resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  
  /*
  Doc: getMedicinesByKeyword. Hace una pet a la BD para obtener las medicinas segun una palabra clave.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByKeyword(keyword : string, page : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-keyword?desde=${page}&nameM=${keyword}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
    /*
  Doc: getMedicinesByKeywordAndByState. Hace una pet a la BD para obtener las medicinas segun una palabra clave y un estado especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByKeywordAndByCountry(keyword : string, idPaisF: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-keyword-and-by-country?desde=${desde}&idPaisF=${idPaisF}&nameM=${keyword}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  /*
  Doc: getMedicinesByKeywordAndByState. Hace una pet a la BD para obtener las medicinas segun una palabra clave y un estado especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByKeywordAndByState(keyword : string, idEstado: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-keyword-and-by-state?desde=${desde}&idEstado=${idEstado}&nameM=${keyword}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
    /*
  Doc: getMedicinesByKeywordAndByState. Hace una pet a la BD para obtener las medicinas segun una palabra clave y un estado especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByKeywordAndByCity(keyword : string, idCiudad: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-keyword-and-by-city?desde=${desde}&idCiudadF=${idCiudad}&nameM=${keyword}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  /*
  Doc: getMedicinesByKeywordAndByState. Hace una pet a la BD para obtener las medicinas segun una palabra clave y un estado especifico.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByKeywordAndByCountryId(keyword : string, idPais: number, desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicines-by-country?desde=${desde}&idPais=${idPais}`;
    this.loadingMedicines = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('medicines-seen-present', resp);
        this.medicines = resp.medicines;
        this.loadingMedicines = false;
        return resp;
      })
    );
  }
  /*** ADMINISTRADOR ***/
  /*
  Doc: updateIsActivMedicineAdm. Hace una pet delete a la BD para deshabilitar un medicamento de un usuario.
  NO HACE FALTA EL TOKEN YA QUE EL USUARIO TIENE PERMISOS DE ADMINISTRADOR.
  Param: obj: medicine: MedicineDatosEditRequeridos
  */
    updateIsActivMedicineAdm(idMedicine: number, isActive: boolean = false) {
      const obj = {
        isActive
      }
      const url = `${this.url}/activate-medicine/${idMedicine}`;
      return this.http.patch(url, obj)
      .pipe(
        // catchError( this.handleError),
        map((resp: MedicineResponse | any) => {
          console.log(resp);
          if(resp.ok){
            // Notificacion de que el medicamento se borro con exito.
            Swal.fire('Correcto', 'El medicamento fue deshabilitado con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }
  /*
  Doc: getMedicinesAllCountriesAdm. Hace una pet a la BD para obtener las medicinas disponibles, de todos los usuarios, paginadas, SIN IMPORTAR SI EL MEDICAMENTO ESTÁ DESHABILITADO O NO DISPONIBLE.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesAllCountriesAdm(desde : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine?desde=${desde}`;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        return resp;
      })
    );
  }
  /*
  Doc: getMedicinesByKeywordAdm. Hace una pet a la BD para obtener las medicinas segun una palabra clave, SIN IMPORTAR QUE EL MEDICAMENTO ESTE DESHABILITADO O NO ESTE DISPONIBLE.
  Return: obj: MedicineGetPublicResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getMedicinesByKeywordAdm(keyword : string, page : number = 0) : MedicineGetPublicResponse | any {
    const url = `${this.url}/medicine-by-keyword?desde=${page}&nameM=${keyword}`;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: MedicineGetPublicResponse | any) => {
        return resp;
      })
    );
  }
  getAllCitiesMedicinesLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-all-cities');
  }
  getAllStatesMedicinesLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-all-states');
  }
  borrarAllCitiesMedicinesLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('medicines-all-cities');
  }
  borrarAllStatesMedicinesLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('medicines-all-states');
  }
  getMedicinesByCityLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-by-city');
  }
  getMedicinesByStateLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-by-state');
  }
  borrarMedicinesByCityLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('medicines-by-city');
  }
  getMedicinesByCategoryIdLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-by-category-id');
  }
  borrarMedicinesByCategoryIdLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('medicines-by-category-id');
  }
  getMedicinesOfUserLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-of-user');
  }
  borrarMedicinesOfUserLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('medicines-of-user');
  }
  updateMedicinesSeenPresentLocalStorage(medicinesSeen: MedicineGetPublicResponse): MedicineGetPublicResponse | any {
    return this.userService.guardarStorage('medicines-seen-present', medicinesSeen);
  }
  getMedicinesSeenPresentLocalStorage(): MedicineGetPublicResponse | any {
    return this.userService.cargarStorage('medicines-seen-present');
  }
  borrarMedicinesSeenPresentLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('medicines-seen-present');
  }




    /* Handle Errors */
    public handleError(error: HttpErrorResponse) {
      console.log(error);
      if (error.error.msg){
      if(error.error.msg === 'No se encontraron preguntas de seguridad según el email especificado') return;
       Swal.fire({
         icon: 'error',
         title: "Error",
         text: error.error.msg
       });
      }
      if(error.status == 0){
        this.loadingMedicines = false;
        this.huboUnError = true;
        console.log('this.loadingMedicines ', this.loadingMedicines);
        Swal.fire({
          icon: 'error',
          title: "Error",
          text: 'Compruebe la conexión a Internet.'
        });
      }
      if (error.error instanceof ErrorEvent) {
         // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
         // The backend returned an unsuccessful response code.
         // The response body may contain clues as to what went wrong.
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
       // Return an observable with a user-facing error message.
      return throwError(
        'Something bad happened; please try again later.');
    }
}
