import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GenderGetResponse } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { UserService } from '../user.service';
import { catchError, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class GenderService {
  private url = environment.baseUrl;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }


  /*
  Doc: getCitiesCallHttp. Hace una pet a la BD para obtener un array de ciudades.
  Return: obj: GenderGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getGendersCallHttp() : GenderGetResponse | any {
    const url = `${this.url}/gender`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: GenderGetResponse | any) => {
        console.log(resp);
        this.userService.guardarStorage('genders', resp);
        return resp;
      })
    );
  }
  getGendersLocalStorage(): GenderGetResponse | any {
    return this.userService.cargarStorage('genders');
  }
  borrarGendersLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('genders');
  }
}
