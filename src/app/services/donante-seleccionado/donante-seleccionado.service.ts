import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Donante, DonanteSeleccionadoGetResponse, DonanteSeleccionadoPostResponse, PeticionDonacion } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class DonanteSeleccionadoService {
  // Variables
  private url = environment.baseUrl;
  donanteSeleccionados: Donante[];
  loadingDonanteSeleccionado: boolean = false;
  huboUnError: boolean = false;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

  /*
  Doc: createDonanteSeleccionado. Hace una pet post a la BD para crear un donante seleccionado.
  Param: obj: (peticionDonacionDatosReq: PeticionDonacionDatosReqCrear
  */
    createDonanteSeleccionado(tokenDelCreadorMedicamento: string, idPDonacionF: number) {
      const obj = {
        token: tokenDelCreadorMedicamento,
        idPDonacionF
      }
      // Los mails funcionan en heroku, en el servidor de hosting maracaibo no esta funcionando.
      const url = `https://backend-medifree.herokuapp.com/donante-seleccionado`;
      return this.http.post(url, obj)
      .pipe(
        // catchError( this.handleError),
        map((resp: DonanteSeleccionadoPostResponse | any) => {
          console.log(resp);
          if(resp.ok){
            Swal.fire('Correcto', 'Ha seleccionado con éxito la persona a donar el medicamento.', 'success');
          }
          return resp;
        })
      );
  
    }

      /*
  Doc: getDSSolicitante. Hace una pet a la BD para obtener las solicitudes de donacion del solicitante, paginadas.
  Return: obj: PeticionDonacionGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getDSSolicitante(idSolicitante: number, desde: number) : DonanteSeleccionadoGetResponse | any {
    const url = `${this.url}/donante-seleccionado-solicitante/${idSolicitante}?desde=${desde}`;
    this.loadingDonanteSeleccionado = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: DonanteSeleccionadoGetResponse | any) => {
        this.donanteSeleccionados = resp.donanteS;
        this.loadingDonanteSeleccionado = false;
        console.log('this.donanteSeleccionados ', this.donanteSeleccionados);
        /*
        this.updatePeticionDonacionLocalStorage(resp);
        this.huboUnError = false;
        this.peticionDonacion = resp.peticionDonacion;
        this.loadingDonanteSeleccionado = false;
        */
        return resp;
      })
    );
  }
        /*
  Doc: getDSCreador. Hace una pet a la BD para obtener las solicitudes de donacion del creador, paginadas.
  Return: obj: PeticionDonacionGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getDSCreador(idSolicitante: number, desde: number) : DonanteSeleccionadoGetResponse | any {
    const url = `${this.url}/donante-seleccionado-creador/${idSolicitante}?desde=${desde}`;
    this.loadingDonanteSeleccionado = true;
    return this.http.get(url)
    .pipe(
      // catchError( this.handleError),
      map((resp: DonanteSeleccionadoGetResponse | any) => {
        // console.log(resp);
        this.userService.guardarStorage('donantes-s-creador', resp);
        this.donanteSeleccionados = resp.donanteS;
        this.loadingDonanteSeleccionado = false;
        console.log('this.donanteSeleccionados ', this.donanteSeleccionados);
        return resp;
      })
    );
  }

                /*
  Doc: deleteDonanteS. Hace una pet delete a la BD para borrar un medicamento de un usuario.
  Param: obj: (medicine: MedicineDatosEditRequeridos
  */
    deleteDonanteS(idDonanteS: number, token: string) {
      const url = `${this.url}/donante-seleccionado/${idDonanteS}?token=${token}`;
      return this.http.delete(url)
      .pipe(
        // catchError( this.handleError),
        map((resp: any) => {
          console.log(resp);
          if(resp.ok){
            let positionMedicamentoABorrarDelArray: number = 0;
            // Conocer la posicion del medicamento borrado.
            for (let i: number = 0; i < this.donanteSeleccionados.length; i++) {
              if (this.donanteSeleccionados[i].idDonanteSeleccionado === idDonanteS) {
                positionMedicamentoABorrarDelArray = i;
                console.log('positionMedicamentoABorrarDelArray! ', positionMedicamentoABorrarDelArray);
                // Eliminar del array de donanteSeleccionados el medicamento borrado.
                this.donanteSeleccionados.splice(positionMedicamentoABorrarDelArray, 1);
                return;
              }
            }
          }
          return resp;
        })
      );
  
    }
}
