import { TestBed } from '@angular/core/testing';

import { DonanteSeleccionadoService } from './donante-seleccionado.service';

describe('DonanteSeleccionadoService', () => {
  let service: DonanteSeleccionadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DonanteSeleccionadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
