import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationsService {

  constructor() { }
    /*
  Doc: Dice si los passwords son iguales.
  Parametros: password1, password2.
  Regresa un booleano o un null.
  */
  sonIguales(campo1: string, campo2: string) {
    return (group: FormGroup) => {
      const pass1 = group.controls[campo1].value;
      const pass2 = group.controls[campo2].value;
      if (pass1 === pass2) {
        return null;
      }
      return {
        sonIguales: true,
      };
    };
  }
 /*
  Doc: Dice si el campo es invalido valiendose de cualquier campo del array [Validator]
  Parametros: campo: string, forma: FormGroup
  Regresa un booleano.
  */
  campoNoEsValido(campo: string, forma: FormGroup): boolean {
    return forma.controls[campo].errors && forma.controls[campo].touched;
  }
  /*
  Doc: Dice si el password esta muy corto o no.
  Param: campo @string
  Return: boolean
  */
  passwordMuyCorto(campo: string, forma: FormGroup): boolean{
    // For debuging:
    // console.log(this.forma.get(campo));
    if(!forma.get(campo).errors){
      return false;
    }
    return forma.get(campo).errors.minlength;
  }
  /*
  Doc: Dice si el password es requerido.
  Param: campo: string, forma: FormGroup
  Return: boolean
  */
  passwordRequerido(campo: string, forma: FormGroup): boolean{
    // For debuging:
    // console.log(forma.get(campo));
    // console.log('forma.get(campo).errors.required ', forma.get(campo).errors.required);
    // console.log('forma.get(campo).touched ', forma.get(campo).touched);
    if(!forma.get(campo).errors){
      return false;
    }
    return forma.get(campo).errors.required && forma.get(campo).touched;
  }

}
