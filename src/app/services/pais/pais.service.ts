import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PaisesGetResponse } from '../../interfaces/interfaces';
import { catchError, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PaisService {
  private url = environment.baseUrl;
  public selectedCountry = '';
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

     /*
  Doc: getCountriesAvailablesCallHttp. Hace una pet a la BD para obtener un array de paises.
  Return: obj: PaisesGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getCountriesAvailablesCallHttp() : PaisesGetResponse | any {
    const url = `${this.url}/pais`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: PaisesGetResponse | any) => {
        console.log(resp);
        this.saveListOfCountries(resp);
        return resp;
      })
    );
  }
  saveListOfCountries(resp: PaisesGetResponse) : void{
    this.userService.guardarStorage('countries', resp);
  }
  /*
  Funcion que guarda en local storage el estado seleccionado y actualiza el valor de la variable selectedCountry.
  */
  saveCountrySelected(selectedCountry: string) : void{
    this.selectedCountry = selectedCountry;
    this.userService.guardarStorage(
      'country-selected',
      this.selectedCountry
    );
  }
  getCountriesLocalStorage(): PaisesGetResponse | any {
    return this.userService.cargarStorage('countries');
  }
  getCountrySelectedId(): string{
    return this.userService.cargarStorage('country-selected');
  }
  borrarPaisesLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('countries');
  }
}
