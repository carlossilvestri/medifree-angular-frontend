import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { EstadoGetResponse } from '../../interfaces/interfaces';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {
  private url = environment.baseUrl;
  public selectedState = '';
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

   /*
  Doc: getStatesByPaisIdCallHttp. Hace una pet a la BD para obtener un array de ciudades.
  Return: obj: EstadoGetResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getStatesByPaisIdCallHttp(idPais: number) : EstadoGetResponse | any {
    const url = `${this.url}/states-by-country?idPaisF=${idPais}`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: EstadoGetResponse | any) => {
        console.log(resp);
        this.saveStatesByCountry(resp);
        return resp;
      })
    );
  }
  getStatesByPaisLocalStorage(): EstadoGetResponse | any {
    return this.userService.cargarStorage('estadosP');
  }
  saveStatesByCountry(resp: EstadoGetResponse) : void{
    this.userService.guardarStorage('estadosP', resp);
  }
  /*
  Funcion que guarda en local storage el estado seleccionado y actualiza el valor de la variable selectedState.
  */
  saveStateSelected(selectedState: string) : void{
    this.selectedState = selectedState;
    this.userService.guardarStorage(
      'state-selected',
      this.selectedState
    );
  }
  getStateSelectedId(): string{
    return this.userService.cargarStorage('state-selected');
  }
  borrarCitiesLocalStorage(): void {
    this.userService.eliminarCampoLocalStorage('estadosP');
  }
}
