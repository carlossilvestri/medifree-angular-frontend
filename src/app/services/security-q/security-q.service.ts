import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { SecurityQDatosRequeridosEdit, SecurityQResponse, User, UserCorto, SecurityQDatosRequeridosCreate } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityQService {
  private url = environment.baseUrl;
  canChangePassword: boolean = false;
  constructor(
    private userService: UserService,
    public router: Router,
    public http: HttpClient,
  ) { }

    /*
  Doc: getSQByIdUserAndTokenCallHttp. Hace una pet a la BD para obtener las preguntas de seg del usuario.
  Return: obj: SecurityQResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getSQByIdUserAndTokenCallHttp(user: User | UserCorto, token: string) : SecurityQResponse | any {
    const url = `${this.url}/qr/get-by-user-token/token?token=${token}`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: SecurityQResponse | any) => {
        // console.log(resp);
        this.userService.guardarStorage('security-questions', resp.qr);
        return resp.qr;
      })
    );
  }
      /*
  Doc: getSQByUserEmailCallHttp. Hace una pet a la BD para obtener las preguntas de seg del usuario.
  Return: obj: SecurityQResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getSQByUserEmailCallHttp(email: string) : SecurityQResponse | any {
    const url = `${this.url}/qr/get-by-email/email?email=${email}`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: SecurityQResponse | any) => {
        // console.log(resp);
        // this.userService.guardarStorage('security-questions', resp.qr);
        return resp.qr;
      })
    );
  }
  /*
  Doc: editSecurityQByUser. Hace una pet a la BD para editar los datos de las preg de seg del usuario.
  Param: obj: (qr: SecurityQDatosRequeridosEdit
  */
  editSecurityQById(qr: SecurityQDatosRequeridosEdit) {
    const url = `${this.url}/qr/${qr.idQr}`;
    return this.http.put(url, qr)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: SecurityQResponse | any) => {
        console.log(resp);
        if(resp.ok){
          this.userService.guardarStorage('security-questions', resp.qr);
          Swal.fire('Correcto', 'Las Preguntas de Seguridad fueron editadas con éxito.', 'success');
        }
        return resp;
      })
    );

  }
    /*
  Doc: createSecurityQByUser. Hace una pet a la BD para crear los datos de las preg de seg del usuario.
  Param: obj: (qr: SecurityQDatosRequeridosCreate
  */
  createSecurityQByUser(qr: SecurityQDatosRequeridosCreate) {
      const url = `${this.url}/qr`;
      return this.http.post(url, qr)
      .pipe(
        catchError( this.userService.handleError),
        map((resp: SecurityQResponse | any) => {
          console.log(resp);
          if(resp.ok){
            this.userService.guardarStorage('security-questions', resp.qr);
            Swal.fire('Correcto', 'Las Preguntas de Seguridad fueron creadas con éxito.', 'success');
          }
          return resp;
        })
      );
  
    }

        /*
  Doc: getSQByIdUserAndTokenCallHttp. Hace una pet a la BD para obtener las preguntas de seg del usuario.
  Return: obj: SecurityQResponse | any.
  Nota: any porque puede que haya un error al enviar la solicitud.
  */
  getTokenToModifyPasswordCallHttp(email: string, r1: string, r2: string) : SecurityQResponse | any {
    const url = `${this.url}/qr/token/get-token-to-modify-pass-by-email-and-answers?email=${email}&r1=${r1}&r2=${r2}`;
    return this.http.get(url)
    .pipe(
      catchError( this.userService.handleError),
      map((resp: SecurityQResponse | any) => {
        console.log(resp);
        this.canChangePassword = true;
        // Se va a necesitar guardar el token en localstorage para usarlo al momento de cambiar la contrasena.
        this.userService.guardarStorage('token', resp.token);
        this.userService.guardarStorage('security-questions', resp.qr);
        return resp.qr;
      })
    );
  }
}
