import { TestBed } from '@angular/core/testing';

import { SecurityQService } from './security-q.service';

describe('SecurityQService', () => {
  let service: SecurityQService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SecurityQService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
