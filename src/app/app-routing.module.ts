import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearCuentaComponent } from './pages/crear-cuenta/crear-cuenta.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { OlvidarpasswordComponent } from './pages/olvidarpassword/olvidarpassword.component';
import { SecurityQuestionsComponent } from './pages/security-questions/security-questions.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { RegisterMedicineComponent } from './components/register-medicine/register-medicine.component';
import { MedicamentoMuestrasComponent } from './components/medicamento-muestras/medicamento-muestras.component';
import { SolicitudDonacionComponent } from './components/solicitud-donacion/solicitud-donacion.component';
import { DetalleMedicamentoComponent } from './components/detalle-medicamento/detalle-medicamento.component';
import { DetalleSolicitudDonacionComponent } from './components/detalle-solicitud-donacion/detalle-solicitud-donacion.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { EditSecurityQComponent } from './components/edit-security-q/edit-security-q.component';
import { MotivoDonacionComponent } from './components/motivo-donacion/motivo-donacion.component';
import { DonantesSeleccionadosComponent } from './components/donantes-seleccionados/donantes-seleccionados/donantes-seleccionados.component';
import { VerificaTokenGuard } from './guards/verifica-token.guard';
import { ChangePasswordGuard } from './guards/change-password/change-password.guard';
import { AdministracionComponent } from './pages/administracion/administracion.component';
import { UsuariosComponent } from './pages/administracion/usuarios/usuarios.component';
import { MedicamentosComponent } from './pages/administracion/medicamentos/medicamentos.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent,
    children: [
      { path: '', component: MedicamentoMuestrasComponent },
      { path: 'edit-user', canActivate: [VerificaTokenGuard], component: EditUserComponent },
      { path: 'edit-security-questions', canActivate: [VerificaTokenGuard], component: EditSecurityQComponent },
      { path: 'registrar-medicina', canActivate: [VerificaTokenGuard], component: RegisterMedicineComponent },
      { path: 'mis-medicamentos', canActivate: [VerificaTokenGuard], component: MedicamentoMuestrasComponent },
      { path: 'edit-medicine/:id', canActivate: [VerificaTokenGuard], component: RegisterMedicineComponent },
      { path: 'detalle-medicamento/:id', component: DetalleMedicamentoComponent },
      { path: 'solicitud-donacion', canActivate: [VerificaTokenGuard], component: SolicitudDonacionComponent },
      { path: 'solicitud-donacion-recibir', canActivate: [VerificaTokenGuard], component: SolicitudDonacionComponent },
      { path: 'solicitud-donacion/:id', component: DetalleSolicitudDonacionComponent },
      { path: 'solicitud-donacion-recibir/:id', component: DetalleSolicitudDonacionComponent },
      { path: 'detalle-solicitud-medicamento/:id', component: DetalleSolicitudDonacionComponent },
      { path: 'motivo-donacion/:id', component: MotivoDonacionComponent },
      { path: 'donantes-seleccionados',  canActivate: [VerificaTokenGuard], component: DonantesSeleccionadosComponent },
      { path: 's-d/:id/d-s/:idDS',  canActivate: [VerificaTokenGuard], component: DetalleSolicitudDonacionComponent },
      { path: 'adm',  canActivate: [VerificaTokenGuard], component: AdministracionComponent },
      { path: 'adm-usuarios',  canActivate: [VerificaTokenGuard], component: UsuariosComponent },
      { path: 'adm-medicamentos',  canActivate: [VerificaTokenGuard], component: MedicamentosComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'crear-cuenta', component: CrearCuentaComponent },
  
  { path: 'forget-password', component: OlvidarpasswordComponent },
  { 
    path: 'security-questions/:email', 
    component: SecurityQuestionsComponent,
    // canActivate: [yourGuard], 
  },
  { 
    path: 'change-password/:email', 
    component: ChangePasswordComponent,
    canActivate: [ChangePasswordGuard], 
  },
  
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
