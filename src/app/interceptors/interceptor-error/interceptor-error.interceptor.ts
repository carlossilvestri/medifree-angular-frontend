import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpClient
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/interfaces/interfaces';
import { Router } from '@angular/router';

@Injectable()
export class InterceptorErrorInterceptor implements HttpInterceptor {
  // Variables
  private url = environment.baseUrl;
  constructor(
    public http: HttpClient,
    public userService: UserService,
    public router: Router,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log('Paso por el interceptor. ');
    return next.handle(request).pipe(
      catchError(this.handleError)
    );
  }

    /* Handle Errors */
    public handleError(error: HttpErrorResponse) {
      console.log(error);
      if (error.error.msg){
      if(error.error.msg === 'No se encontraron preguntas de seguridad según el email especificado') return;
       Swal.fire({
         icon: 'error',
         title: "Error",
         text: error.error.msg
       });
      }
      if(error.status == 0){
        Swal.fire({
          icon: 'error',
          title: "Error",
          text: 'Compruebe la conexión a Internet.'
        });
      }
       // Ver si se vencio el token.
       if(error.error.errors.message === 'jwt expired'){
         console.log('err ', error.error.errors.message);
         Swal.fire({
           icon: 'error',
           title: "Error",
           text: 'Sesion expirada.'
         });
         this.router.navigateByUrl('/login');
         return throwError(error);
       }
      if (error.error instanceof ErrorEvent) {
         // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
         // The backend returned an unsuccessful response code.
         // The response body may contain clues as to what went wrong.
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
       // Return an observable with a user-facing error message.
      return throwError(
        'Something bad happened; please try again later.');
    }

}
