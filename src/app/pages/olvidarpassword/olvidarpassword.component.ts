import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationsService } from 'src/app/services/validations/validations.service';

@Component({
  selector: 'app-olvidarpassword',
  templateUrl: './olvidarpassword.component.html',
  styleUrls: ['./olvidarpassword.component.scss']
})
export class OlvidarpasswordComponent implements OnInit {
  email: string = '';
  forma: FormGroup;
  constructor(
    public router: Router,
    private fb: FormBuilder,
    private validationService: ValidationsService
  ) { }

  ngOnInit(): void {
    this.inicializarFormulario();
  }

  irA(){
    // Resaltar errores si los hay
    this.forma.markAllAsTouched();
    /*  
    // For debugging
    console.log("this.forma.get('emailU').value ", this.forma.get('emailU').value);
    return;
    */
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    const email = this.forma.get('emailU').value;
    this.router.navigateByUrl(`/security-questions/${email}`);
  }
  inicializarFormulario(){
    this.forma = this.fb.group({
        emailU          : ['', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
                                  Validators.required,
                                  Validators.email]
                                 ],
    }
    )
  }
    /*
  Doc: Validacion general que te devuelve un boolean para ver si un campo tiene o no algo.
  */
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }
  get correo() {
    return this.forma.get('emailU').value;
  }
}
