import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { GetUser } from 'src/app/interfaces/interfaces';
import Swal from 'sweetalert2';
import { Subject, Observable } from 'rxjs';
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: [
    './usuarios.component.scss',
    '../../../components/solicitud-donacion/solicitud-donacion.component.scss',
  ],
})
export class UsuariosComponent implements OnInit {
  users: GetUser;
  usuarioABuscar: string = '';
  cargandoDatos: boolean = false;
  pagina : number = 0;
  public eventOnPaginaReset: Subject<void> = new Subject<void>();
  constructor(public userService: UserService) {}

  ngOnInit(): void {
    this.inicializarCampos();
  }
  emitEventToChild() {
    this.eventOnPaginaReset.next()
  }
  updatePage(event: number) {
    // console.log("pagina ", this.pagina++);
    // console.log('Evento updatePage llamado: ', event);
    this.llenarDatosUsuarios(event);
  }
  inicializarCampos(): void {
    this.llenarDatosUsuarios();
  }
  /**
    llenarDatosAdm() : void
    Permite obtener los datos de la Base de datos para mostrar las cantidades disponibles, como por ejemplo: Usuarios, medicamentos, categorias, paises, etc.
   */
  llenarDatosUsuarios(desde : number = 0): void {
    // Se esta cargando.
    this.cargandoDatos = true;
    this.userService.getUsersAllCountries(desde).subscribe((resp: GetUser) => {
      this.users = resp;
      this.cargandoDatos = false;
    });
  }
  buscarUsuario() {
    if (this.usuarioABuscar.trim().length > 0) {
      // Se esta cargando.
      this.cargandoDatos = true;
      this.userService
        .getUsersAllCountriesByKeyword(this.usuarioABuscar, 0)
        .subscribe((resp: GetUser) => {
          this.users = resp;
          this.cargandoDatos = false;
        });
    } else {
      Swal.fire('Error', 'Debe ingresar al menos un cáracter.', 'error');
    }
  }
  habilitarODeshabilitarUsuario(id: number, habilitarDeshabilitar: boolean) {
    Swal.fire({
      title: '¿Estás seguro/a?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // Desloguearse y eliminar del localstorage varios campos mediante el servicio del logout.
        this.userService
          .habilitarODeshabilitarUsuario(id, habilitarDeshabilitar)
          .subscribe((resp) => {
            this.llenarDatosUsuarios();
            Swal.fire('Correcto', 'Usuario modificado.', 'success');
          });
      }
    });
  }
  permisosDeAdm(id: number, habilitarDeshabilitar: boolean) {
    // Desloguearse y eliminar del localstorage varios campos mediante el servicio del logout.
    this.userService
      .permisosDeAdm(id, habilitarDeshabilitar)
      .subscribe((resp) => {
        this.llenarDatosUsuarios();
        Swal.fire('Correcto', 'Usuario modificado.', 'success');
      });
  }
  /**
   * alCambiarPermisos() : void.
   * Cambia los permisos de un usuario en particular.
   * @param newValue : $event
   */
  alCambiarPermisos(newValue, idUser: number): void {
    // For debugging
    console.log('alCambiarPermisos newValue ', newValue.target.value);
    console.log('idUser ', idUser);
    this.userService
      .permisosDeAdm(idUser, newValue.target.value)
      .subscribe((resp) => {
        this.llenarDatosUsuarios();
        Swal.fire('Correcto', 'Usuario modificado.', 'success');
      });
  }
  /**
   * alCambiarOrden() : void
   * Funcion que llama a otras funciones para ordenar los usuarios:
   * Si valorSelect '1' => Ordenar alfabeticamente. '2' => Ordenar por Adm. '3' => Ordenar por no Adm.
   * @param event selectEvent
   */
  alCambiarOrden(event){
    let valorSelect : string = event.target.value;
    switch (valorSelect) {
      case '1':
        this.ordenarUsuarioAlfabeticamente(); 
        break;
      case '2':
          this.ordenarPorAdm(); 
          break;
      case '3':
          this.ordenarPorNoAdm(); 
          break;
      default:
        break;
    }
  }
  ordenarUsuarioAlfabeticamente() {
    // Ordenar alfabaeticamente.
    this.users.users.sort((a, b) => {
      if (a.namesU < b.namesU) {
        return -1;
      }
      if (a.namesU > b.namesU) {
        return 1;
      }
      return 0;
    });
  }
  ordenarPorAdm() {
    // Ordenar alfabaeticamente.
    this.users.users.sort((a, b) => {
      if (
        a.isSuperAdministrator.toString() > b.isSuperAdministrator.toString()
      ) {
        return -1;
      }
      if (
        a.isSuperAdministrator.toString() < b.isSuperAdministrator.toString()
      ) {
        return 1;
      }
      return 0;
    });
  }
  ordenarPorNoAdm() {
    // Ordenar alfabaeticamente.
    this.users.users.sort((a, b) => {
      if (
        a.isSuperAdministrator.toString() < b.isSuperAdministrator.toString()
      ) {
        return -1;
      }
      if (
        a.isSuperAdministrator.toString() > b.isSuperAdministrator.toString()
      ) {
        return 1;
      }
      return 0;
    });
  }
}
