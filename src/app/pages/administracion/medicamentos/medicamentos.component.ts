import { Component, OnInit } from '@angular/core';
import { MedicineGetPublicResponse } from 'src/app/interfaces/interfaces';
import { MedicinesService } from '../../../services/medicines/medicines.service';
import { UserService } from '../../../services/user.service';
import Swal from 'sweetalert2';
import { Subject, Observable } from 'rxjs';
@Component({
  selector: 'app-medicamentos',
  templateUrl: './medicamentos.component.html',
  styleUrls: ['./medicamentos.component.scss', '../../../components/solicitud-donacion/solicitud-donacion.component.scss', '../usuarios/usuarios.component.scss']
})
export class MedicamentosComponent implements OnInit {
  medicinesR: MedicineGetPublicResponse;
  medicamentoABuscar: string = '';
  cargandoDatos: boolean = false;
  pagina : number = 0;
  constructor(
    public medicineService: MedicinesService,
    public userService: UserService
    ) {}

  ngOnInit(): void {
    this.inicializarCampos();
  }
  public eventOnPaginaReset: Subject<void> = new Subject<void>();
  emitEventToChild() {
    this.eventOnPaginaReset.next()
  }
  inicializarCampos(): void {
    this.llenarMedicamentoTodosLosPaises();
  }
  updatePage(event: number) {
    // console.log("pagina ", this.pagina++);
    // console.log('Evento updatePage llamado: ', event);
    this.llenarMedicamentoTodosLosPaises(event);
  }
    /**
    llenarMedicamentoTodosLosPaises() : void
    Permite obtener los datos de la Base de datos para mostrar los medicamentos.
   */
    llenarMedicamentoTodosLosPaises(desde = 0): void {
      // Se esta cargando.
      this.cargandoDatos = true;
      this.medicineService.getMedicinesAllCountriesAdm(desde).subscribe((resp: MedicineGetPublicResponse) => {
        this.medicinesR = resp;
        this.cargandoDatos = false;
      });
    }
    habilitarODeshabilitarMedicamento(id: number, habilitarDeshabilitar: boolean) {
      Swal.fire({
        title: '¿Estás seguro/a?',
        // text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
      }).then((result) => {
        if (result.isConfirmed) {
          // Desloguearse y eliminar del localstorage varios campos mediante el servicio del logout.
          this.medicineService
            .updateIsActivMedicineAdm(id, habilitarDeshabilitar)
            .subscribe((resp) => {
              this.llenarMedicamentoTodosLosPaises();
              Swal.fire('Correcto', 'Usuario modificado.', 'success');
            });
        }
      });
    }
    buscarMedicamento() {
      if (this.medicamentoABuscar.trim().length > 0) {
        // Se esta cargando.
        this.cargandoDatos = true;
        this.medicineService
          .getMedicinesByKeywordAdm(this.medicamentoABuscar, 0)
          .subscribe((resp: MedicineGetPublicResponse) => {
            this.medicinesR = resp;
            this.cargandoDatos = false;
          });
      } else {
        Swal.fire('Error', 'Debe ingresar al menos un cáracter.', 'error');
      }
    }
    alCambiarOrden(event){
      let valorSelect : string = event.target.value;
      switch (valorSelect) {
        case '1':
          this.ordenarMedicamentoAlfabeticamente() ; 
          break;
        default:
          break;
      }
    }
    ordenarMedicamentoAlfabeticamente() {
      // Ordenar alfabaeticamente.
      this.medicinesR.medicines.sort((a, b) => {
        if (a.nameM < b.nameM) {
          return -1;
        }
        if (a.nameM > b.nameM) {
          return 1;
        }
        return 0;
      });
    }
}
