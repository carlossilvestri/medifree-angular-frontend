import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { GetDatosAdm } from 'src/app/interfaces/interfaces';
@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['../../components/solicitud-donacion/solicitud-donacion.component.scss', './administracion.component.scss']
})
export class AdministracionComponent implements OnInit {
  datosAdm : GetDatosAdm;
  cargandoDatos: boolean = false;
  constructor(
    public userService: UserService,
  ) { }

  ngOnInit(): void {
    this.inicializarCampos()
  }
  inicializarCampos() : void{
    this.llenarDatosAdm();
  }
  /**
    llenarDatosAdm() : void
    Permite obtener los datos de la Base de datos para mostrar las cantidades disponibles, como por ejemplo: Usuarios, medicamentos, categorias, paises, etc.
   */
  llenarDatosAdm() : void{
    // Se esta cargando.
    this.cargandoDatos = true;
    this.userService
      .obtenerDatosDeAdministracion()
      .subscribe((resp: GetDatosAdm) => {
        this.datosAdm = resp;
         this.cargandoDatos = false;
      });
  }

}
