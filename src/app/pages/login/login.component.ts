import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginDatosRequeridos, User } from 'src/app/interfaces/interfaces';
import Swal from 'sweetalert2';
import { CitiesService } from 'src/app/services/cities/cities.service';
import { Observable, forkJoin } from 'rxjs';
import { GenderService } from 'src/app/services/gender/gender.service';
import { SecurityQService } from 'src/app/services/security-q/security-q.service';
import { CategoryMedService } from 'src/app/services/category-med/category-med.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { DonanteSeleccionadoService } from 'src/app/services/donante-seleccionado/donante-seleccionado.service';
import { EstadoService } from '../../services/estado/estado.service';
import { PaisService } from '../../services/pais/pais.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // Variables
  forma: FormGroup;
  cargando: boolean = false;
  constructor(
    private userService: UserService,
    private cityService: CitiesService,
    private stateService: EstadoService,
    private countryService: PaisService,
    private genderService: GenderService,
    private securityQService: SecurityQService,
    private donanteSService: DonanteSeleccionadoService,
    public router: Router,
    private categoryMedService: CategoryMedService,
    private medicineService: MedicinesService,
  ) { }

  ngOnInit(): void {
    this.forma = new FormGroup(
      {
        emailU: new FormControl('', [Validators.required, Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]),
        password: new FormControl('', [Validators.required]),
      }
    );
  }

  /* Funciones */
  onLogin(): void {
    // Resaltar errores si los hay
    Object.values(this.forma.controls).forEach((control) => {
      if (control instanceof FormGroup) {
        Object.values(control.controls).forEach((control2) =>
          control2.markAsTouched()
        );
      }
      control.markAsTouched();
    });
    // For debugging
    // console.log('this.forma ', this.forma);
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    // Es valido. Crear el objeto.
    const usuarioLoginDatos: LoginDatosRequeridos = {
      emailU: this.forma.value.emailU,
      password: this.forma.value.password
    };
    // For debugging.
    // console.log('usuarioLoginDatos ', usuarioLoginDatos);
    // Cargando...
    Swal.showLoading();
    // Llamar al servicio.
    const loginCall = this.userService.login(usuarioLoginDatos),
          // citiesGetCall = this.cityService.getCitiesCallHttp(),
          // paisesGetCall = this.countryService.getCountriesAvailablesCallHttp(),
          genderGetCall = this.genderService.getGendersCallHttp(),
          categoriesMedGetCall = this.categoryMedService.getCategoriesCallHttp();
    // Hacer las 2 llamadas al mismo tiempo.
    forkJoin([ loginCall, genderGetCall, categoriesMedGetCall]).subscribe((results ) => {
          /*
          // For debugging.
          console.log('results  ', results );
          */
          // results[0] is our loginCall
          // results[1] is our citiesGetCall
          this.cargarCamposExtrasDependientesDelToken();
        });
  }
  /*
  Doc: mostrarNotifBienvenida.
  */
  mostrarNotifBienvenida(): void{
    Swal.fire({
      title: '¡Bienvenido!',
      text: '¡Encuentra/Dona tus medicinas gratis!',
      imageUrl: 'assets/img/1er-fase/medifree-fondo-celeste.png',
      imageWidth: 330,
      imageHeight: 200,
      imageAlt: 'MediFree',
    })
  }
  /**
   *   Doc: cargarCamposExtrasDependientesDelToken. Llamada adicionales a la api
   */
  cargarCamposExtrasDependientesDelToken(){
    // Obtener token.
    const token: string = this.userService.cargarStorage('token'),
    user: User = this.userService.cargarStorage('user'),
    // medicinesByCitytCall = this.medicineService.getMedicinesByCity(user.idCiudadF, 0),
    medicinesByStateCall = this.medicineService.getMedicinesByState(user.ciudades.idEstadoF, 0),
    preguntasDeSeguridadGetCall = this.securityQService.getSQByIdUserAndTokenCallHttp(user, token);

    // Llamadas a la API.
    forkJoin([ preguntasDeSeguridadGetCall, medicinesByStateCall]).subscribe(() => {
      this.mostrarNotifBienvenida();// Dejar de mostrar el loading y manda un msj de bienvenida.
      this.router.navigate(['/home']);
    });
  }
  // Deprecrated
  quitarLoading(){
    const swalLoading = document.getElementsByClassName('swal2-container');
    console.log('swalLoading ', swalLoading);
    if(swalLoading){
      let padre = swalLoading[0].parentNode;
		  padre.removeChild(swalLoading[0]);
    }
  }
  /* Getters */
  get correoNoValido(): boolean {
    return this.forma.get('emailU').invalid && this.forma.get('emailU').touched;
  }
  get pass1NoValido(): boolean {
    return (
      this.forma.get('password').invalid && this.forma.get('password').touched
    );
  }
}
