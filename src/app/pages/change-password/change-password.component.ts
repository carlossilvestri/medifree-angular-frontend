import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  forma: FormGroup;
  constructor(
    private fb: FormBuilder,
    private validationService: ValidationsService,
    private userService: UserService,
    private router: Router 
  ) { }

  ngOnInit(): void {
    this.inicializarFormulario();
  }
  inicializarFormulario(){
    this.forma = this.fb.group({
        password        : ['', [Validators.required, Validators.minLength(5)]],
        password2       : ['', [Validators.required, Validators.minLength(5)]],
    },
    {
      validators: this.validationService.sonIguales('password', 'password2'),
    }
    )
  }
  // FUNCTIONS
  changePassword(){
    // Resaltar errores si los hay
    this.forma.markAllAsTouched();
    
    // For debugging.
    console.log('this.forma ', this.forma);
    
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    // Revisar si existe un token en localstorage.
    const token: string = this.userService.cargarStorage('token');
    if(!token){
      return;
    }
    // Obtener los valores a enviar.
    const password = this.forma.get('password').value,
          password2 = this.forma.get('password2').value;
    // Preguntar si esta seguro
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // Crear mediante el servicio.
        this.userService.changeOnlyPassword(token, password, password2).subscribe(
          (resp) => {
          console.log('resp ', resp);
          this.router.navigateByUrl('/login');
        });
      }
    });
  }
  /*
  Doc: Validacion general que te devuelve un boolean para ver si un campo tiene o no algo.
  */
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }
  // Getters
  get password1MuyCorta(): boolean {
    return this.forma.get('password').errors.minlength;
  }
  /*
  Doc: Dice si el password esta muy corto o no.
  Param: campo @string
  Return: boolean
  */
  passwordMuyCorto(campo: string): boolean{
    // For debuging:
    // console.log(this.forma.get(campo));
    return this.validationService.passwordMuyCorto(campo, this.forma);
  }
    /*
  Doc: Dice si el password esta muy corto o no.
  Param: campo @string
  Return: boolean
  */
  passwordRequerido(campo: string): boolean{
    // For debuging:
    // console.log(this.forma.get(campo));
    return this.validationService.passwordRequerido(campo, this.forma);
  }
  get pass2NoValido(): boolean {
    const pass1 = this.forma.get('password').value;
    const pass2 = this.forma.get('password2').value;
    // console.log('this.forma.get.pass2.touched ', this.forma.get('password2').touched);
    if(pass1.length === 0) return false; // No se escribio nada en el campo password1
    // if (!this.forma.get('password2').touched) return false;
    return pass1 === pass2 && this.forma.get('password').touched
      ? false
      : true;
  }
  get pass1NoValido(): boolean {
    const pass1 = this.forma.get('password').value;
    const pass2 = this.forma.get('password2').value;
    // console.log('this.forma.get.pass2.touched ', this.forma.get('password2').touched);
    if(pass2.length === 0) return false; // No se escribio nada en el campo password1
    // if (!this.forma.get('password2').touched) return false;
    return pass1 === pass2 && this.forma.get('password2').touched
      ? false
      : true;
  }
}
