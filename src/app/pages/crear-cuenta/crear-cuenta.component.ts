import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Ciudades, CiudadesGetResponse, EstadoGetResponse, GenderGetResponse, PaisesGetResponse, Sexos, UserAlCrear } from 'src/app/interfaces/interfaces';
import Swal from 'sweetalert2';
import { CitiesService } from 'src/app/services/cities/cities.service';
import { GenderService } from 'src/app/services/gender/gender.service';
import { EstadoService } from 'src/app/services/estado/estado.service';
import { PaisService } from 'src/app/services/pais/pais.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';

@Component({
  selector: 'app-crear-cuenta',
  templateUrl: './crear-cuenta.component.html',
  styleUrls: ['./crear-cuenta.component.scss'],
})
export class CrearCuentaComponent implements OnInit {
  forma: FormGroup;
  gendersR: GenderGetResponse;
  ciudadesR: CiudadesGetResponse;
  estadosR: EstadoGetResponse;
  paisesR: PaisesGetResponse;
  selectGender: string = '';
  selectCity: string = '';
  constructor(
    private userService: UserService,
    private citiesService: CitiesService,
    public stateService: EstadoService,
    public countriesService: PaisService,
    private genderService: GenderService,
    public medicinesService: MedicinesService,
    private validationService: ValidationsService,
     public router: Router,
     private fb: FormBuilder
     ) {}

  ngOnInit(): void {
    this.iniciarlizarCampos();
  }

  /* Funciones */
  iniciarlizarCampos(): void{
    this.fetchCountriesR();
    this.fetchGenders();
    this.inicializarFormulario();
    // this.llenarFormulario();
  }
  fetchCitiesR(){
      // Angular necesita los datos inmetiadamente, pero no hay ciudades en el local storage asi que miestras carga la solicitud con las ciudades se coloca un array vacio.
      // this.inicializarCiudadVacia();
      // Llamar End Point para obtener las ciudades
      this.citiesService.getCitiesByStateIdCallHttp(Number(this.stateService.selectedState)).subscribe((resp : CiudadesGetResponse | any) => {
        console.log('resp ', resp);
        this.ciudadesR = resp;
      });
  }
  fetchStatesR() {
    // this.estadosR = this.stateService.getStatesByPaisLocalStorage();
    // if (!this.estadosR) {
    // Angular necesita los datos inmetiadamente, pero no hay ciudades en el local storage asi que miestras carga la solicitud con las ciudades se coloca un array vacio.
    // this.inicializarEstadoVacio();
    // this.inicializarCiudadVacia();
    // Llamar End Point para obtener las ciudades
    this.stateService
      .getStatesByPaisIdCallHttp(Number(this.countriesService.selectedCountry))
      .subscribe((resp: EstadoGetResponse | any) => {
        console.log('resp ', resp);
        this.estadosR = resp;
      });
    // }
  }
  fetchCountriesR() {
      // Angular necesita los datos inmetiadamente, pero no hay ciudades en el local storage asi que miestras carga la solicitud con las ciudades se coloca un array vacio.
      this.iniciarlizarPaisEstadoCiudad();
      // this.inicializarCiudadVacia();
      // Llamar End Point para obtener las ciudades
      this.countriesService
        .getCountriesAvailablesCallHttp()
        .subscribe((resp: PaisesGetResponse | any) => {
          console.log('resp ', resp);
          this.paisesR = resp;
        });
  }
  fetchGenders(){
    this.gendersR = this.genderService.getGendersLocalStorage();
    if(!this.gendersR){
      // Angular necesita los datos inmetiadamente, pero no hay generos en el local storage asi que miestras carga la solicitud con los generos se coloca un array vacio.
      this.inicializarGenderVacia();
      // Llamar End Point para obtener las ciudades
      this.genderService.getGendersCallHttp().subscribe((resp : GenderGetResponse | any) => {
        console.log('resp ', resp);
        this.gendersR = resp;
      });
    }
  }
  onChangeState(newValue) {
    // this.cityService.selectedCity = newValue;
    this.stateService.selectedState = newValue.target.value;
    console.log('onChangeCountry newValue.target.value ', newValue.target.value);
    this.idEstado.setValue(newValue.target.value, {
      onlySelf: true
    });
    // Actualizar storage de la ciudad seleccionada.
    this.stateService.saveStateSelected(this.stateService.selectedState);
    this.fetchCitiesR();
  }
  onChangeCountry(newValue) {
    this.inicializarEstadoVacio();
    this.inicializarCiudadVacia();
    console.log('onChangeCountry newValue.target.value ', newValue.target.value);
    this.idPais.setValue(newValue.target.value, {
      onlySelf: true
    })
    // this.cityService.selectedCity = newValue;
    this.countriesService.selectedCountry = newValue.target.value;
    // Actualizar storage de la ciudad seleccionada.
    this.countriesService.saveCountrySelected(
      this.countriesService.selectedCountry
    );
    this.fetchStatesR();
  }
  iniciarlizarPaisEstadoCiudad(){
    this.inicializarPaisVacio();
    this.inicializarEstadoVacio();
    this.inicializarCiudadVacia();
  }
  inicializarFormulario(){
    this.forma = this.fb.group({
        namesU          : ['', [Validators.required]],
        lastNamesU      : ['', [Validators.required]],
        emailU          : ['', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
                                  Validators.required,
                                  Validators.email]
                                 ],
        password        : ['', [Validators.required,
                                Validators.minLength(3),
                          ]     ],
        password2       : ['', [Validators.required]],
        identificationU : ['', [Validators.required,
                                Validators.pattern('[V-]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$'),
                          ]     ],
        dateOfBirth     : ['', [Validators.required]],
        directionU      : ['', [Validators.required]],
        tlf1            : ['', [Validators.required,
                                Validators.pattern('[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$'),
                          ]     ],
        tlf2            : [''],
        letraIdentificacion: ['V', [Validators.required]],
        idCiudadF       : ['', [Validators.required]],
        idEstado        : ['', [Validators.required]],
        idPais          : ['', [Validators.required]],
        idGenderF       : ['', [Validators.required]], 
    },
    {
      validators: this.sonIguales('password', 'password2'),
    }
    )
  }
  inicializarCiudadVacia(){
    this.ciudadesR = {
      ok: false,
      ciudades: [{
        idCiudad:   0,
        nameCiudad: '',
        isVisible:  true,
        createdAt:  new Date(),
        updatedAt:  new Date(),
        idEstadoF: 0,
        estado: {
          idEstado: 0,
          nombreEstado: '',
          isVisible: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          idPaisF: 0,
          paises: {
            idPais: 0,
            nameP: '',
            isVisible: true,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
        },
      }]
    }
  }
  inicializarGenderVacia(){
    this.gendersR = {
      ok: false,
      genders: [{
        idGender:   0,
        nameGender: '',
        isVisible:  true,
        createdAt:  new Date(),
        updatedAt:  new Date(),
      }]
    }
  }
  inicializarEstadoVacio() {
    this.estadosR = {
      ok: true,
      cantidadEstados: 0,
      estados: [
        {
          idEstado: 0,
          nombreEstado: '',
          isVisible: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          idPaisF: 0,
          paises: {
            idPais: 0,
            nameP: "",
            isVisible: true,
            createdAt: new Date(),
            updatedAt: new Date(),
        }
        },
      ],
    };
  }
  inicializarPaisVacio() {
    this.paisesR = {
      ok: true,
      paises: [
          {
              idPais: 0,
              nameP: "",
              isVisible: true,
              createdAt: new Date(),
              updatedAt: new Date(),
          }
      ]
  };
  }
  /*
  Doc: Dice si los passwords son iguales.
  Parametros: password1, password2.
  Regresa un booleano o un null.
  */
  sonIguales(campo1: string, campo2: string) {
    return (group: FormGroup) => {
      const pass1 = group.controls[campo1].value;
      const pass2 = group.controls[campo2].value;
      if (pass1 === pass2) {
        return null;
      }
      return {
        sonIguales: true,
      };
    };
  }
  /* 
  Doc: Permite llenar la informacion del formulario de 
  */
  llenarFormulario(): void {
    this.forma.setValue({
      namesU: 'Carlos',
      lastNamesU: 'Silvestri',
      emailU: 'carlos2@gmail.com',
      password: '1234',
      password2: '1234',
      identificationU: 'V-26551730',
      dateOfBirth: '2021-05-13',
      directionU: 'Calle 70 con av 26',
      tlf1: '+5812312321',
      tlf2: '+5812312321',
      idCiudadF: '1',
      idPais: '1',
      idGenderF: '1',
    });
  }
  onSubmit(): void {
    // Resaltar errores si los hay
    this.forma.markAllAsTouched();
    
    // For debugging.
    console.log('this.forma ', this.forma);

    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    // Es valido. Crear el objeto.
    const usuarioACrear: UserAlCrear = {
      emailU: this.forma.value.emailU,
      password: this.forma.value.password,
      password2: this.forma.value.password2,
      namesU: this.forma.value.namesU,
      lastNamesU: this.forma.value.lastNamesU,
      identificationU: this.forma.value.letraIdentificacion + '-' + this.forma.value.identificationU,
      dateOfBirth: this.forma.value.dateOfBirth,
      directionU: this.forma.value.directionU,
      tlf1: this.forma.value.tlf1,
      tlf2: this.forma.value.tlf2,
      idCiudadF: this.forma.value.idCiudadF,
      idGenderF: this.forma.value.idGenderF,
    };
    /*
    // For debugging.
    console.log('usuarioACrear ', usuarioACrear);
    console.log('this.forma ', this.forma);
    return;
    */
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // Crear mediante el servicio.
        this.userService.crearUsuario(usuarioACrear).subscribe((resp) => {
          console.log('resp ', resp);
          this.forma.reset(); // Borra los campos del formulario.
          this.router.navigate(['/login']);
        });
      }
    });
    console.log('this.forma ', this.forma);
  }
  // Choose city using select dropdown
  changeCity(e) {
      // console.log(e.target.value);
      this.idCiudadF.setValue(e.target.value, {
        onlySelf: true
      })
  }
  changeGender(e) {
    // console.log(e.target.value);
    this.idGenderF.setValue(e.target.value, {
      onlySelf: true
    })
  }
  /*
  Doc: Validacion general que te devuelve un boolean para ver si un campo tiene o no algo.
  */
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }

  get idCiudadF() {
    return this.forma.get('idCiudadF');
  }
  get idEstado() {
    return this.forma.get('idEstado');
  }
  get idPais() {
    return this.forma.get('idPais');
  }
  get idGenderF() {
    return this.forma.get('idGenderF');
  }
  /* Getters para saber si un campo es valido */
  get nombreNoValido(): boolean {
    return this.forma.get('namesU').invalid && this.forma.get('namesU').touched;
  }
  get apellidoNoValido(): boolean {
    return (
      this.forma.get('lastNamesU').invalid &&
      this.forma.get('lastNamesU').touched
    );
  }
  get fechaNoValido(): boolean {
    return (
      this.forma.get('dateOfBirth').invalid &&
      this.forma.get('dateOfBirth').touched
    );
  }
  get correoNoValido(): boolean {
    return this.forma.get('emailU').invalid && this.forma.get('emailU').touched;
  }
  get tlf1NoValido(): boolean {
    return this.forma.get('tlf1').invalid && this.forma.get('tlf1').touched;
  }
  get identificacionNoValido(): boolean {
    return (
      this.forma.get('identificationU').invalid &&
      this.forma.get('identificationU').touched
    );
  }
  get ciudadNoValido(): boolean {
    return (
      this.forma.get('idCiudadF').invalid && this.forma.get('idCiudadF').touched
    );
  }
  get direccionNoValido(): boolean {
    return (
      this.forma.get('directionU').invalid && this.forma.get('directionU').touched
    );
  }
  get generoNoValido(): boolean {
    return (
      this.forma.get('idGenderF').invalid && this.forma.get('idGenderF').touched
    );
  }
  get pass1NoValido(): boolean {
    return (
      this.forma.get('password').invalid && this.forma.get('password').touched
    );
  }
  get passwordMuyCorta(): boolean {
    return this.forma.get('password').errors.minlength;
  }
  get pass2NoValido(): boolean {
    const pass1 = this.forma.get('password').value;
    const pass2 = this.forma.get('password2').value;
    // console.log('this.forma.get.pass2.touched ', this.forma.get('password2').touched);
    if (!this.forma.get('password2').touched) return false;
    return pass1 === pass2 && this.forma.get('password2').touched
      ? false
      : true;
  }
}
