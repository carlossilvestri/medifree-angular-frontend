import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Qr } from 'src/app/interfaces/interfaces';
import { SecurityQService } from 'src/app/services/security-q/security-q.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-security-questions',
  templateUrl: './security-questions.component.html',
  styleUrls: ['./security-questions.component.scss']
})
export class SecurityQuestionsComponent implements OnInit {
  forma: FormGroup;
  email: string = '';
  securityQ: Qr;
  loading: boolean = true;
  constructor(
    private validationService: ValidationsService,
    private fb: FormBuilder,
    private sqService: SecurityQService,
    private activatedRoute: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.fetchParams();
    this.cargarPreguntasDeSeguridad();
  }
  fetchParams(){
    this.activatedRoute.params.subscribe((params: any) => {
      // tslint:disable-next-line: no-string-literal
      this.email = params['email'];
      console.log('email ', this.email);
    });
  }
  cargarPreguntasDeSeguridad(){
    this.loading = true;
    this.sqService
      .getSQByUserEmailCallHttp(this.email)
      .subscribe(
        (resp) => {
          this.securityQ = resp;
          // console.log('this.securityQ ', this.securityQ);
          this.inicializarFormulario();
          this.loading = false;
        },
        (err) => {
          console.log('err ', err);
           Swal.fire({
             icon: 'error',
             title: "Error",
             text: "Correo no registrado/Sin preguntas de seguridad registradas."
           });
          this.router.navigateByUrl(`/forget-password`);
        }
      );
  }
  inicializarFormulario(){
        this.forma = this.fb.group({
          q1                : [this.securityQ.q1 , [Validators.required]],
          q2                : [this.securityQ.q2, [Validators.required]],
          r1                : ['', [Validators.required]],
          r2                : ['', [Validators.required] ],
      }
      )
  }
  /*
  Doc: Validacion general que te devuelve un boolean para ver si un campo tiene o no algo.
  */
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }
  notificarError(){
    Swal.fire({
      icon: 'error',
      title: "Error",
      text: "Correo no registrado."
    });
  }
  sendAnswers(){
        // Resaltar errores si los hay
        this.forma.markAllAsTouched();
    
        // For debugging.
        console.log('this.forma ', this.forma);
        let r1 = this.forma.value.r1;
        let r2 = this.forma.value.r2;
        // Revisar si el formulario es valido.
        if (this.forma.invalid) {
          return;
        }
        // Preguntar si el usuario esta seguro.
        Swal.fire({
          title: '¿Estás seguro?',
          // text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí',
        }).then((result) => {
          if (result.isConfirmed) {
            // Crear mediante el servicio.
            this.sqService.getTokenToModifyPasswordCallHttp(this.email, r1, r2).subscribe(
              (resp) => {
              // console.log('resp ', resp);
              this.forma.reset(); // Borra los campos del formulario.
              this.router.navigate([`change-password/${this.email}`]);
              },
            );
          }
        });
  }
}
