import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  Medicine,
  MedicineGetPublicResponse,
  MedicineLong,
} from 'src/app/interfaces/interfaces';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { HelpersService } from '../../services/helpers/helpers.service';
import { ImageService } from '../../services/image/image.service';

@Component({
  selector: 'app-detalle-medicamento',
  templateUrl: './detalle-medicamento.component.html',
  styleUrls: ['./detalle-medicamento.component.scss'],
})
export class DetalleMedicamentoComponent implements OnInit {
  idMedicineParam: number = 0;
  medicinesSeenPresent: MedicineGetPublicResponse;
  medicine: MedicineLong;
  medicineCategory: string = '';
  imageObject: Array<object> = [
    {
      image:
        'https://vale-dev.s3.us-west-2.amazonaws.com/product/null-Torta%20de%20Chocolate%201Kg/tortadechocolate.jpg',
      thumbImage:
        'https://vale-dev.s3.us-west-2.amazonaws.com/product/null-Torta%20de%20Chocolate%201Kg/tortadechocolate.jpg'  
    },
    {
      image:
        'https://vale-dev.s3.us-west-2.amazonaws.com/product/null-Torta%20de%20Vainilla%201Kg/Vanilla%20Cake%20SyS.jpg', // Support base64 image
      thumbImage:
        'https://vale-dev.s3.us-west-2.amazonaws.com/product/null-Torta%20de%20Vainilla%201Kg/Vanilla%20Cake%20SyS.jpg', // Support base64 image,
        order: 1
      },
  ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private medicineService: MedicinesService,
    private router: Router,
    public helperService: HelpersService,
    public imageService: ImageService
) {}

  ngOnInit(): void {
    this.iniciarInit();
  }
  iniciarInit() {
    this.cargarMedicamento();
  }
  cargarMedicamento() {
    this.activatedRoute.params.subscribe((params: any) => {
      // tslint:disable-next-line: no-string-literal
      this.idMedicineParam = Number(params['id']);
    });
    this.medicinesSeenPresent =
      this.medicineService.getMedicinesSeenPresentLocalStorage();
    if (!this.medicinesSeenPresent || this.medicinesSeenPresent == undefined) {
      this.router.navigateByUrl('/home');
    }
    // Conocer la posicion del medicamento borrado.
    for (
      let i: number = 0;
      i < this.medicinesSeenPresent.medicines.length;
      i++
    ) {
      if (
        this.medicinesSeenPresent.medicines[i].idMedicine ===
        this.idMedicineParam
      ) {
        this.medicine = this.medicinesSeenPresent.medicines[i];
        this.imageService.getImagesCallHttp("medicines",this.idMedicineParam ).subscribe();
        return;
      }
    }
    this.router.navigateByUrl('/home');
  }
}
