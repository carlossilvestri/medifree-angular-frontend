import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSecurityQComponent } from './edit-security-q.component';

describe('EditSecurityQComponent', () => {
  let component: EditSecurityQComponent;
  let fixture: ComponentFixture<EditSecurityQComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSecurityQComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSecurityQComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
