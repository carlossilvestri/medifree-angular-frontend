import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  Qr,
  SecurityQDatosRequeridosEdit,
  SecurityQDatosRequeridosCreate,
  User,
  UserCorto,
} from 'src/app/interfaces/interfaces';
import { SecurityQService } from 'src/app/services/security-q/security-q.service';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-security-q',
  templateUrl: './edit-security-q.component.html',
  styleUrls: ['../edit-user/edit-user.component.scss'],
})
export class EditSecurityQComponent implements OnInit {
  // Variables
  user: User | UserCorto;
  token: string = '';
  securityQuestions: Qr;
  securityQDatosRequeridos: SecurityQDatosRequeridosEdit;
  sQDatosCrear: SecurityQDatosRequeridosCreate;
  forma: FormGroup;
  constructor(
    private router: Router,
    private userService: UserService,
    private securityQService: SecurityQService,
    private validationService: ValidationsService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.inicializarCampos();
  }

  inicializarCampos(): void {
    // this.id = this.route.snapshot.params['id'];
    this.fetchSecurityQuestions();
    this.inicializarFormulario();
  }

  /*
  Doc: fetchSecurityQuestions. Obtiene los datos del usuario guardado en LocalStorage.
  Retorna: un obj de tipo LoginResponse para el caso del usuario.
  */
  fetchSecurityQuestions(): void {
    this.user = this.userService.cargarStorage('user');
    this.token = this.userService.cargarStorage('token');
    this.securityQuestions =
      this.userService.cargarStorage('security-questions');
    if (!this.securityQuestions) {
      this.iniciarlizarObjnRSinDatos();
      // Obtener los datos de la API.
      /* this.securityQService.getSQByIdUserAndTokenCallHttp(this.user, this.token).subscribe((results ) => {
        this.securityQuestions = results.qres;
      }); */
    }
    // For debugging
    console.log('this.user ', this.user);
    console.log('securityQuestions ', this.securityQuestions);
  }
  inicializarFormulario() {
    this.forma = this.fb.group({
      q1: [this.securityQuestions.q1, [Validators.required]],
      q2: [this.securityQuestions.q2, [Validators.required]],
      r1: [this.securityQuestions.r1, [Validators.required]],
      r2: [this.securityQuestions.r2, [Validators.required]],
    });
  }
  iniciarlizarObjnRSinDatos() {
    this.securityQuestions = {
      idQr: 0,
      q1: '',
      q2: '',
      r1: '',
      r2: '',
      idUsuarioF: '0',
    };
  }

  enviarPregSeg() {
    
    console.log('this.securityQuestions ', this.securityQuestions);
    // Resaltar errores si los hay
    this.forma.markAllAsTouched();
    
    // For debugging.
    console.log('this.forma ', this.forma);
    
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    this.securityQDatosRequeridos = {
      idQr: this.securityQuestions.idQr,
      q1: this.forma.value.q1,
      q2: this.forma.value.q2,
      r1: this.forma.value.r1,
      r2: this.forma.value.r2,
      token: this.token,
    };
    this.sQDatosCrear = {
      q1: this.forma.value.q1,
      q2: this.forma.value.q2,
      r1: this.forma.value.r1,
      r2: this.forma.value.r2,
      idUsuarioF: this.user.idUser,
      token: this.token,
    };
    Swal.fire({
      title: '¿Estás seguro/a?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // El usuario esta editando las preg o las esta creando?
        const hayPreguntas =
          this.userService.cargarStorage('security-questions');
        if (!hayPreguntas || hayPreguntas.length === 0) {
          this.securityQService
            .createSecurityQByUser(this.sQDatosCrear)
            .subscribe((resp) => {
              console.log('resp ', resp);
              this.router.navigate(['/home']);
            });
        } else {
          // Editar Preg Seguridad.
          this.securityQService
            .editSecurityQById(this.securityQDatosRequeridos)
            .subscribe((resp) => {
              console.log('resp ', resp);
              this.router.navigate(['/home']);
            });
        }
      }
    });
  }
  /*
  Doc: Validacion general que te devuelve un boolean para ver si un campo tiene o no algo.
  */
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }
}
