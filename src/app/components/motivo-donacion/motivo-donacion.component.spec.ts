import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotivoDonacionComponent } from './motivo-donacion.component';

describe('MotivoDonacionComponent', () => {
  let component: MotivoDonacionComponent;
  let fixture: ComponentFixture<MotivoDonacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotivoDonacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotivoDonacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
