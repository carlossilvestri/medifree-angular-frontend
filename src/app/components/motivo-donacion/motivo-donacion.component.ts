import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MedicineGetPublicResponse, MedicineLong, PeticionDonacionDatosReqCrear, User } from 'src/app/interfaces/interfaces';
import { ImageService } from 'src/app/services/image/image.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { PeticionDonacionService } from 'src/app/services/peticion-donacion/peticion-donacion.service';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-motivo-donacion',
  templateUrl: './motivo-donacion.component.html',
  styleUrls: ['./motivo-donacion.component.scss', '../detalle-medicamento/detalle-medicamento.component.scss']
})
export class MotivoDonacionComponent implements OnInit {
  idMedicineParam: number = 0;
  medicinesSeenPresent: MedicineGetPublicResponse;
  medicine: MedicineLong;
  medicineCategory: string = '';
  motivoDonacion: string = '';
  peticionDonacionDatosReqCrear: PeticionDonacionDatosReqCrear;
  token: string = '';
  forma: FormGroup;
  user: User;
  constructor(
    private activatedRoute: ActivatedRoute,
    private medicineService: MedicinesService,
    private userService: UserService,
    private peticionDonacionService: PeticionDonacionService,
    private validationService: ValidationsService,
    private router: Router,
    private fb: FormBuilder,
    public imageService: ImageService
  ) { }

  ngOnInit(): void {
    this.iniciarInit();
  }
  iniciarInit(){
    this.fetchToken();
    this.fetchUser();
    this.cargarMedicamento();
    this.inicializarFormulario();
  }
  cargarMedicamento(){
    this.activatedRoute.params.subscribe((params: any) => {
      // tslint:disable-next-line: no-string-literal
      this.idMedicineParam = Number(params['id']);
    });
    // Cargar las fotos del
    this.imageService.getImagesCallHttp("medicines",this.idMedicineParam ).subscribe();
    this.medicinesSeenPresent = this.medicineService.getMedicinesSeenPresentLocalStorage();
    if(!this.medicinesSeenPresent || this.medicinesSeenPresent == undefined){
      this.router.navigateByUrl('/home');
    }
    // Conocer la posicion del medicamento borrado.
    for (let i: number = 0; i < this.medicinesSeenPresent.medicines.length; i++) {
      if (this.medicinesSeenPresent.medicines[i].idMedicine === this.idMedicineParam) {
        this.medicine = this.medicinesSeenPresent.medicines[i];
        return;
      }
    }
    this.router.navigateByUrl('/home');
  }
  fetchToken(): void {
    this.token = this.userService.cargarStorage('token');
  }
  fetchUser(): void {
    this.user = this.userService.cargarStorage('user');
  }
  inicializarFormulario() {
    this.forma = this.fb.group({
      msjDonacion: ['', [Validators.required]],
    });
  }
  crearPeticionDonacion() {

    // Resaltar errores si los hay
    this.forma.markAllAsTouched();
    
    // For debugging.
    console.log('this.forma ', this.forma);
    
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    if(!this.user){
      Swal.fire('Error', 'Debe iniciar sesión para solicitar un medicamento.', 'error');
      return;
    }
    console.log('this.medicine ', this.medicine);
    // Revisar si el usuario es el creador del medicamento.
    if(this.medicine.idUsuarioF === this.user.idUser){
      // Notificar que no puede solicitar la donacion.
      Swal.fire('Error', 'No puede solicitar una donación a usted mismo.', 'error');
      return;
    }
    this.peticionDonacionDatosReqCrear = {
      token:       this.token,
      msjDonacion: this.forma.value.msjDonacion,
      idMedicineF: this.idMedicineParam
    };
    Swal.fire({
      title: '¿Estás seguro/a?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // Colocar spinner de carga.
        // Swal.showLoading(); // Cargando... NO ESTA FUNCIONANDO EL LOADING.
          // Crear la peticion de donacion/ solicitud de donacion
          this.peticionDonacionService
            .createPeticionDonacion(this.peticionDonacionDatosReqCrear)
            .subscribe((resp) => {
              console.log('resp ', resp);
              this.router.navigate(['/home']);
            });
      }
    });
  }
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }

}
