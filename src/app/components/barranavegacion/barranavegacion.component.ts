import { Component, OnDestroy, OnInit } from '@angular/core';
import { Categoria, User } from 'src/app/interfaces/interfaces';
import { CategoryMedService } from 'src/app/services/category-med/category-med.service';
import { CitiesService } from 'src/app/services/cities/cities.service';
import { EstadoService } from 'src/app/services/estado/estado.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { PaisService } from 'src/app/services/pais/pais.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-barranavegacion',
  templateUrl: './barranavegacion.component.html',
  styleUrls: ['./barranavegacion.component.scss'],
})
export class BarranavegacionComponent implements OnInit, OnDestroy {
  esconder: boolean = true;
  // categorias: string[] = ['Categoria 1', 'Categoria 2' ,'Categoria 3'];
  categorias: Categoria[];
  anchoPantalla: number;
  usuarioLogueado: boolean = false;
  usuario: User;
  constructor(
    public userService: UserService,
    private categoryMedService: CategoryMedService,
    public medicinesService: MedicinesService,
    public cityService: CitiesService,
    public stateService: EstadoService,
    public countriesService: PaisService
  ) {}

  ngOnInit(): void {
    this.pregUsuarioLogueado();
    this.iniciarlizarategorias();
    this.windowWidthListener();
    if (this.anchoPantalla > 420) {
      this.openSlideMenu();
    } else {
      this.mIzqListener();
    }
    this.dropDownBtnListener();
  }
  ngOnDestroy(): void {
    window.removeEventListener('resize', this.setAncho);
  }
  /* Funciones */
  pregUsuarioLogueado() {
    // El usuario esta logueado? Si esta logueado verificar si ya hay cargadas medicinas segun su ciudad.
    let usuario: User = this.userService.cargarStorage('user');
    // El usuario no esta logueado, mostrar los medicamentos de todas las ciudades.
    if (!usuario) {
      this.usuarioLogueado = false;
      // console.log('this.usuarioLogueado ', this.usuarioLogueado);
    } else {
      this.usuarioLogueado = true;
      this.usuario = usuario;
      // console.log('this.usuarioLogueado ', this.usuarioLogueado);
    }
  }
  openSlideMenu(): void {
    // Como esta parte es JavaScript puro, se debe realizar un peq retrazo al cargar la funcion ya que angular aun no carga los elemento.
    setTimeout(() => {
      document.getElementById('menu').classList.toggle('barra-active');
      let arrayImgs = document.getElementsByClassName('img-efecto');
      // console.log('arrayimg ', arrayImgs);
      for (let i = 0; i < arrayImgs.length; i++) {
        arrayImgs[i].classList.toggle('animate__backOutLeft');
      }
      let arrayElemtsEfect2 = document.getElementsByClassName('img-efecto2');
      for (let i = 0; i < arrayElemtsEfect2.length; i++) {
        arrayElemtsEfect2[i].classList.toggle('animate__fadeIn');
      }
      document.getElementById('burger-btn').classList.toggle('d-none');
      this.esconderEspacio();
    }, 300);
  }
  closeSlideMenuAnchoImporta(desdeLogo: boolean = false): void {
    if (this.anchoPantalla < 420) {
      let arrayImgs = document.getElementsByClassName('img-efecto');
      for (let i = 0; i < arrayImgs.length; i++) {
        arrayImgs[i].classList.toggle('animate__backOutLeft');
      }
      let arrayElemtsEfect2 = document.getElementsByClassName('img-efecto2');
      for (let i = 0; i < arrayElemtsEfect2.length; i++) {
        arrayElemtsEfect2[i].classList.toggle('animate__fadeIn');
      }
      document.getElementById('menu').classList.toggle('barra-active');
      document.getElementById('burger-btn').classList.toggle('d-none');
      this.esconderEspacio();
    }
    this.medicinesService.page = 0;
    if (desdeLogo) {
      this.cargarMedicamentos();
    }
    // this.cargarMedicamentos();
  }
  closeSlideMenu(): void {
    let arrayImgs = document.getElementsByClassName('img-efecto');
    for (let i = 0; i < arrayImgs.length; i++) {
      arrayImgs[i].classList.toggle('animate__backOutLeft');
    }
    let arrayElemtsEfect2 = document.getElementsByClassName('img-efecto2');
    for (let i = 0; i < arrayElemtsEfect2.length; i++) {
      arrayElemtsEfect2[i].classList.toggle('animate__fadeIn');
    }
    document.getElementById('menu').classList.toggle('barra-active');
    document.getElementById('burger-btn').classList.toggle('d-none');
    this.esconderEspacio();
  }
  dropDownBtnListener(): void {
    setTimeout(() => {
      var dropdown = document.getElementsByClassName('dropdow-funcion');
      var i;
      for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener('click', function () {
          this.classList.toggle('active');
          var dropdownContent = this.nextElementSibling;
          if (dropdownContent.classList.contains('animate__zoomIn')) {
            dropdownContent.classList.add('animate__zoomOut');
            dropdownContent.classList.toggle('animate__zoomIn');
            setTimeout(() => {
              dropdownContent.classList.add('d-none');
            }, 200);
          } else {
            dropdownContent.classList.add('animate__zoomIn');
            dropdownContent.classList.toggle('animate__zoomOut');
            setTimeout(() => {
              dropdownContent.classList.remove('d-none');
            }, 100);
          }
        });
      }
    }, 500);
  }
  esconderEspacio(): void {
    if (this.esconder) {
      this.esconder = false;
    } else {
      this.esconder = true;
    }
    this.mIzqListener();
  }
  mIzqListener(): void {
    // Seleccionar las clases que tengan  vw-100-cont vw-74-cont
    var vw100 = document.getElementsByClassName('vw-100-cont');
    var vw74 = document.getElementsByClassName('vw-74-cont');
    // Colocar la clase vw-100-cont si esta escondido y colocar
    // Si esta escondida la barra azul de medifree...
    if (this.esconder) {
      // Quitar clase, agregar la otra
      for (var i = 0; i < vw74.length; i++) {
        vw74[i].classList.add('vw-100-cont');
        vw74[i].classList.remove('vw-74-cont');
      }
    } else {
      // Se esta en un disp grande.
      console.log('this.anchoPantalla dentro de func ', this.anchoPantalla);
      if (this.anchoPantalla > 420) {
        for (var i = 0; i < vw100.length; i++) {
          vw100[i].classList.add('vw-74-cont');
          vw100[i].classList.remove('vw-100-cont');
        }
      }
    }
  }
  windowWidthListener(): void {
    this.setAncho();
    window.addEventListener('resize', this.setAncho);
  }
  setAncho(): void {
    const contentId: any = document.querySelector('#content');
    this.anchoPantalla = contentId.offsetWidth;
    console.log('this.anchoPantalla ', this.anchoPantalla);
    this.mIzqListener;
  }
  iniciarlizarategorias() {
    // LLenar las categorias de los medicamentos.
    const categories: Categoria[] =
      this.categoryMedService.getCategoriesLocalStorage();
    /*
    // For debugging.
    console.log('categories ', categories);
    */
    // no hay categorias cargadas en el local storage
    if (!categories || categories.length === 0) {
      // Hacer la llamada al end point para obtener las categorias.
      this.categoryMedService.getCategoriesCallHttp().subscribe(
        (resp) => {
          this.categorias = resp.categorias;
          // Es posible que se haya llamado al end point pero que no hayan categorias en la BD por ello inicializar el objeto de categorias adecuadamente.
          if (this.categorias.length === 0) {
            this.categorias =
              this.categoryMedService.iniciarlizarObjCategoriasSinDatos();
          }
          // console.log('this.categorias ', this.categorias);
        },
        // Si hay errores
        (error) => {
          this.categorias =
            this.categoryMedService.iniciarlizarObjCategoriasSinDatos();
        }
      );
      // this.categorias = this.categoryMedService.iniciarlizarObjCategoriasSinDatos();
    } else {
      this.categorias = categories;
    }
  }
  /*
  Doc: Llama al servicio de medicamentos en donde ucambia el arreglo de medicines que se muestra en el home por lo que devuelve el end point de medicinas filtradas por una categoria en particular.
  */
  cargarMedicamentosSegunCategoryId(idCategoria: number) {
    // Hay una ciudad seleccionada?
    if (this.cityService.selectedCity.length > 0) {
      // Cargar los medicamentos por suna categoria y ciudad especifica.
      this.medicinesService
        .getMedicinesByCategoryIdAndByCity(
          idCategoria,
          Number(this.cityService.selectedCity),
          0
        )
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
        });
    } else if (this.stateService.selectedState.length > 0) {
      // Cargar los medicamentos por suna categoria y estado especifico.
      this.medicinesService
        .getMedicinesByCategoryIdAndByState(
          idCategoria,
          Number(this.stateService.selectedState),
          0
        )
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
        });
    } else if (this.countriesService.selectedCountry.length > 0) {
      // Cargar los medicamentos por suna categoria y pais especifico.
      this.medicinesService
        .getMedicinesByCategoryIdAndByCountry(
          idCategoria,
          Number(this.countriesService.selectedCountry),
          0
        )
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
        });
    } else {
      // Cargar los medicamentos por suna categoria y de todos los paises.
      this.medicinesService
        .getMedicinesByCategoryIdAllCountries(idCategoria)
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
        });
    }
  }
  /*
  Doc: Logout. Permite borrar la info de localstorage para desloguear el usuario.
  */
  logoutUser(): void {
    Swal.fire({
      title: '¿Estás seguro/a?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // Desloguearse y eliminar del localstorage varios campos mediante el servicio del logout.
        this.userService.logout();
      }
    });
  }
  /*
  Doc: Al dar click al logo la idea es que si el usuario esta logueado que le muestre las ciudades de su id, pero si no esta logueado que muestre la de todas las ciudades.
  */
  cargarMedicamentos() {
    // El usuario esta logueado? Si esta logueado verificar si ya hay cargadas medicinas segun su ciudad.
    let usuario: User = this.userService.cargarStorage('user');
    let ciudadEspecifica: string = '';
    ciudadEspecifica = this.userService.cargarStorage('city-selected');
    // El usuario no esta logueado, mostrar los medicamentos de todas las ciudades.
    if (!usuario) {
      if (ciudadEspecifica) {
        if (ciudadEspecifica.length > 0) {
          this.cargarMedicinaPorCiudadEspecifica(Number(ciudadEspecifica));
        } else {
          this.cargarMedicinasPorTodosLosPaises();
        }
      } else {
        this.cargarMedicinasPorTodosLosPaises();
      }
      return;
    }
    if (usuario) {
      this.userService.guardarStorage('city-selected', '');
      this.cityService.selectedCity = '';
      // For debugging.
      // console.log('this.cityService.selectedCity ', this.cityService.selectedCity);
      this.cargarMedicinasPorTodosLosPaises();
    }
  }
  cargarMedicinasPorTodosLosPaises() {
    this.medicinesService.getMedicinesAllCountries(0).subscribe(
      (resp) => {
        console.log('resp ', resp);
      },
      (err) => {
        console.log('err ', err);
        this.medicinesService.loadingMedicines = false;
        this.medicinesService.huboUnError = true;
      }
    );
  }
  cargarMedicinaPorCiudadEspecifica(ciudadEspecifica: number) {
    this.medicinesService
      .getMedicinesByCity(Number(ciudadEspecifica), 0)
      .subscribe(
        (resp) => {
          console.log('resp en barranavegacion ', resp);
        },
        (err) => {
          console.log('err ', err);
          this.medicinesService.loadingMedicines = false;
          this.medicinesService.huboUnError = true;
        }
      );
  }
}
