import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonantesSeleccionadosComponent } from './donantes-seleccionados.component';

describe('DonantesSeleccionadosComponent', () => {
  let component: DonantesSeleccionadosComponent;
  let fixture: ComponentFixture<DonantesSeleccionadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonantesSeleccionadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonantesSeleccionadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
