import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Donante, User } from 'src/app/interfaces/interfaces';
import { DonanteSeleccionadoService } from 'src/app/services/donante-seleccionado/donante-seleccionado.service';
import { PeticionDonacionService } from 'src/app/services/peticion-donacion/peticion-donacion.service';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';

@Component({
  selector: 'app-donantes-seleccionados',
  templateUrl: './donantes-seleccionados.component.html',
  styleUrls: ['./donantes-seleccionados.component.scss', '../../solicitud-donacion/solicitud-donacion.component.scss']
})
export class DonantesSeleccionadosComponent implements OnInit {
  titulo: string = 'Donantes Seleccionados';
  token: string = '';
  user: User;
  solicitudRecibir: boolean = false;
  rutaLink: string = ''; 
  constructor(
    private userService: UserService,
    public peticionDonacion: PeticionDonacionService,
    public donanteSeleccionado: DonanteSeleccionadoService,
    private validationService: ValidationsService,
    private fb: FormBuilder,
    public router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.iniciarlizarInit();
  }
  iniciarlizarInit() {
      this.fetchToken();
      this.fetchDonantesSeleccionadosDelCreador();
  }
  fetchDonantesSeleccionadosDelCreador() {
    this.user = this.userService.cargarStorage('user');
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    this.donanteSeleccionado
      .getDSCreador(this.user.idUser, 0)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
        },
        (err) => {
          console.log('err ', err);
          this.donanteSeleccionado.huboUnError = true;
        }
      );
  }
  fetchToken(){
    this.token = this.userService.cargarStorage('token');
  }
  irA(idDS: number, idPDonacion: number){
    this.router.navigateByUrl(`/home/s-d/${idPDonacion}/d-s/${idDS}`);
  }
  elegirParaDarDonacion(idPDonacion: number){
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    this.donanteSeleccionado
      .createDonanteSeleccionado(this.token, idPDonacion)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
        },
        (err) => {
          console.log('err ', err);
        }
      );
  }

}
