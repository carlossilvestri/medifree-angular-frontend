import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  Medicine,
  MedicineGetPublicResponse,
  User,
  UserCorto,
} from 'src/app/interfaces/interfaces';
import { EstadoService } from 'src/app/services/estado/estado.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment.prod';
import { ImageService } from '../../services/image/image.service';

@Component({
  selector: 'app-medicamento-muestras',
  templateUrl: './medicamento-muestras.component.html',
  styleUrls: ['./medicamento-muestras.component.scss'],
})
export class MedicamentoMuestrasComponent implements OnInit {
  // Variables
  user: User;
  editMode: boolean = false;
  token: string = '';
  paramCategoryId: string = '';
  paramCityId: string = '';
  idPais: number = 1; // idPais de Venezuela.
  url: string = environment.baseUrl;
  constructor(
    public medicinesService: MedicinesService,
    private userService: UserService,
    private estadoService: EstadoService,
    public router: Router,
    private route: ActivatedRoute,
    public imageService: ImageService,
  ) {}

  ngOnInit(): void {
    this.iniciarInit();
  }
  iniciarInit(): void {
    this.cargarMedicamentos();
  }
  updatePage(event: number) {
    // console.log('Evento updatePage llamado: ', event);
    this.medicinesService.page = event;
    this.cargarMedicamentos();
  }
  cargarMedicamentos(): void {
    this.paramCategoryId = this.route.snapshot.params.categoryId; // Devuelve un string o un undefined
    this.paramCityId = this.route.snapshot.params.cityId; // Devuelve un string o un undefined
    /*
    // For debugging
    console.log('this.router.url. ', this.router.url);
    console.log('this.paramCategoryId  ', this.paramCategoryId);
    console.log('this.paramCityId  ', this.paramCityId);
    */
    // Si el usuario esta en la ruta del home
    switch (this.router.url) {
      case '/home':
        this.editMode = false;
        break;
      case '/home/mis-medicamentos':
        this.editMode = true;
        break;
      default:
        this.editMode = false;
        break;
    }
    // El usuario esta logueado? Si esta logueado verificar si ya hay cargadas medicinas segun su ciudad.
    this.user = this.userService.cargarStorage('user');
    this.userService.usuario = this.userService.cargarStorage('user');
    // console.log('this.user en cargarMedicamentos ', this.user);
    // El usuario no esta logueado, mostrar los medicamentos de todas las ciudades.
    // if (!this.user) {
    //   console.log('NO hay usuario');
    //   this.cargarMedicamentosTodosLosPaises();
    //   return;
    // }
    // El usuario esta logueado.
    // Si se esta en editMode.
    if (this.editMode) {
      this.cargarMedicamentosSegunElTokenDelUsuario();
    }
    // Si no se esta en editMode.
    // console.log(
    //   'this.medicinesService.medicines.length ',
    //   this.medicinesService.medicines.length
    // );
    // if (!this.editMode && this.medicinesService.medicines.length === 0) {
    if (!this.editMode) {
      // Buscar por todas las ciudades?
      /*
      // Con ciudades @Deprecrated
      let ciudadEspecifica: string = '';
      this.userService.cargarStorage('city-selected');
      ciudadEspecifica = this.userService.cargarStorage('city-selected');
      if(!ciudadEspecifica){
        this.cargarMedicamentosTodosLosPaises();
      }
      */
     // Con estados
     let estadoEspecifico: string = '';
     estadoEspecifico = this.estadoService.getStateSelectedId();
     if(!estadoEspecifico){
       this.cargarMedicamentosTodosLosPaises();
     }
      if(estadoEspecifico){
        if (estadoEspecifico.length > 0) {
          //Buscar por alguna ciudad especifica?
          console.log("Buscar por cargarMedicamentosSegunElEstadoDelUsuarioLogueado");
          if(this.user){
            this.cargarMedicamentosSegunElEstadoDelUsuarioLogueado(
              Number(this.user.ciudades.idEstadoF),
              this.medicinesService.page
            ); 
          }else{
            // Cargar los medicamentos segun el estado seleccionado.
            this.cargarMedicamentosSegunElEstadoDelUsuarioLogueado(
              Number(estadoEspecifico),
              this.medicinesService.page
            );
          }
        } else {
          console.log("Buscar por cargarMedicamentosTodosLosPaises");
          this.cargarMedicamentosTodosLosPaises();
        }
      }else{
        this.cargarMedicamentosTodosLosPaises();
      }
    }
  }
  // Borra un medicamento.
  borrarMedicamentoDelUsuario(idMedicine: number) {
    this.token = this.userService.cargarStorage('token');
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        this.medicinesService
          .deleteMedicineOfUser(idMedicine, this.token)
          .subscribe((resp) => {
            // console.log('resp ', resp);
          });
      }
    });
  }
  // Borra un medicamento.
  deshabilitarMedicamentoDelUsuario(idMedicine: number) {
    this.token = this.userService.cargarStorage('token');
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        this.medicinesService
          .updateIsActivMedicineOfUser(idMedicine, this.token, false)
          .subscribe((resp) => {
            // console.log('resp ', resp);
          });
      }
    });
  }
  cargarMedicamentosTodosLosPaises(): void {
    this.medicinesService.huboUnError = false;
    this.medicinesService.getMedicinesAllCountries(this.medicinesService.page).subscribe(
      (resp) => {
        console.log('resp ', resp);
      },
      (err) => {
        console.log('err ', err);
        this.medicinesService.loadingMedicines = false;
        this.medicinesService.huboUnError = true;
      }
    );
  }
  cargarMedicamentosTodasLosEstados(): void {
    this.medicinesService.huboUnError = false;
    this.medicinesService.getMedicinesAllStates(this.idPais, this.medicinesService.page).subscribe(
      (resp) => {
        // console.log('resp ', resp);
      },
      (err) => {
        // console.log('err ', err);
        this.medicinesService.loadingMedicines = false;
        this.medicinesService.huboUnError = true;
      }
    );
  }
  cargarMedicamentosSegunLaCiudadDelUsuarioLogueado(
    idCiudad: number,
    page: number = 0
  ) {
    this.medicinesService.huboUnError = false;
    this.medicinesService.getMedicinesByCity(idCiudad, page).subscribe(
      (resp) => {
        // console.log('resp ', resp);
      },
      (err) => {
        // console.log('err ', err);
        this.medicinesService.loadingMedicines = false;
        this.medicinesService.huboUnError = true;
      }
    );
  }
  cargarMedicamentosSegunElEstadoDelUsuarioLogueado(
    idEstado: number,
    page: number = 0
  ) {
    this.medicinesService.huboUnError = false;
    this.medicinesService.getMedicinesByState(idEstado, page).subscribe(
      (resp) => {
        // console.log('resp ', resp);
      },
      (err) => {
        // console.log('err ', err);
        this.medicinesService.loadingMedicines = false;
        this.medicinesService.huboUnError = true;
      }
    );
  }
  cargarMedicamentosSegunElTokenDelUsuario() {
    this.token = this.userService.cargarStorage('token');
    this.medicinesService.huboUnError = false;
    this.medicinesService.getMedicinesOfUserId(this.token, this.medicinesService.page).subscribe(
      (resp) => {
        // console.log('resp ', resp);
      },
      (err) => {
        // console.log('err ', err);
        this.medicinesService.loadingMedicines = false;
        this.medicinesService.huboUnError = true;
      }
    );
  }
}
