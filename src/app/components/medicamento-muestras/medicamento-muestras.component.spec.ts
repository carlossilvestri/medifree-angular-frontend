import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicamentoMuestrasComponent } from './medicamento-muestras.component';

describe('MedicamentoMuestrasComponent', () => {
  let component: MedicamentoMuestrasComponent;
  let fixture: ComponentFixture<MedicamentoMuestrasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicamentoMuestrasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicamentoMuestrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
