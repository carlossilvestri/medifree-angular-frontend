import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CiudadesGetResponse,
  GenderGetResponse,
  LoginResponse,
  EditUserNoImage,
  User,
  UserCorto,
  EstadoGetResponse,
  PaisesGetResponse,
  EditUserByIdResponse,
} from 'src/app/interfaces/interfaces';
import { CitiesService } from 'src/app/services/cities/cities.service';
import { EstadoService } from 'src/app/services/estado/estado.service';
import { ImageService } from 'src/app/services/image/image.service';
import { PaisService } from 'src/app/services/pais/pais.service';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';
import Swal from 'sweetalert2';
import { ImgGetResponse } from '../../interfaces/interfaces';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit {
  // Variables:
  forma: FormGroup;
  user: User;
  ciudadesR: CiudadesGetResponse;
  estadosR: EstadoGetResponse;
  paisesR: PaisesGetResponse;
  gendersR: GenderGetResponse;
  selectedCity: string = '';
  selectedState: string = '';
  selectedCountry: string = '';
  selectedGender: string = '';
  fechaFormateada: string = '';
  token: string = '';
  selectedFile: File = null;
  previsualizarImg: string = 'assets/img/botones/user.svg';
  idImage: number = 0;
  constructor(
    private route: ActivatedRoute,
    private validationService: ValidationsService,
    private citiesService: CitiesService,
    public stateService: EstadoService,
    public countriesService: PaisService,
    private router: Router,
    private userService: UserService,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    public imageService: ImageService
  ) {}

  ngOnInit(): void {
    this.inicializarCampos();
  }
  inicializarCampos(): void {
    // this.id = this.route.snapshot.params['id'];
    this.fetchLoginR();
    this.fetchCountriesR();
    this.fetchGenders();
    this.fetchToken();
    this.fetchImg();
    this.inicializarFormulario();
    /*
    // For debugging
    console.log('this.selectedCity ', this.selectedCity);
    */
  }
  /*
  Doc: fetchLoginR. Obtiene los datos del usuario guardado en LocalStorage.
  Retorna: un obj de tipo LoginResponse para el caso del usuario.
  */
  fetchLoginR(): void {
    this.user = this.userService.cargarStorage('user');
    if (!this.user) {
      this.iniciarlizarObjLoginRSinDatos();
    }
    this.fechaFormateada = this.datePipe.transform(
      new Date(this.user.dateOfBirth),
      'yyyy-MM-dd'
    );
    let arrayDatefechaFormateada: string[] = this.fechaFormateada.split('-');
    let ultimoDigitoFecha: number = Number(arrayDatefechaFormateada[2]) + 1;
    arrayDatefechaFormateada[2] = ultimoDigitoFecha.toString();
    this.fechaFormateada = arrayDatefechaFormateada.join('-');
    /*
    // For debugging
    console.log('this.user ', this.user);
    console.log('arrayDatefechaFormateada ', arrayDatefechaFormateada);
    console.log('fechaFormateada ', this.fechaFormateada);
    console.log('fechaFormateada ', this.fechaFormateada);
    console.log('arrayDatefechaFormateada ', arrayDatefechaFormateada);
    console.log('ultimoDigitoFecha ', ultimoDigitoFecha);
    console.log('fechaFormateada ', this.fechaFormateada);
    */
  }
  fetchToken(): void {
    this.token = this.userService.cargarStorage('token');
  }
  fetchImg(): void {
    this.imageService.getImagesCallHttp("users", this.user.idUser).subscribe(
      (resp: ImgGetResponse) =>{
        if(resp.imagenes.length > 0){
          console.log("resp LA QUE ME IMPORTA ", resp);
          this.previsualizarImg = this.imageService.url + "/upload/users/" + resp.imagenes[0].nameImage;
          this.idImage = resp.imagenes[0].idImage;
          // Hay problemas con este end point al editar que no guarda el main en la img, asi que por ahora el codigo de arriba.
          resp.imagenes.forEach(element => {
            if(element.mainImage){
              this.previsualizarImg = this.imageService.url + "/upload/users/" + element.nameImage;
              this.idImage = element.idImage;
            }
          });
        }
      }
    );
  }
  fetchCountriesR() {
    // Angular necesita los datos inmetiadamente, pero no hay ciudades en el local storage asi que miestras carga la solicitud con las ciudades se coloca un array vacio.
    this.iniciarlizarPaisEstadoCiudad();
    // this.inicializarCiudadVacia();
    // Llamar End Point para obtener las ciudades
    this.countriesService
      .getCountriesAvailablesCallHttp()
      .subscribe((resp: PaisesGetResponse | any) => {
        console.log('resp ', resp);
        this.paisesR = resp;
      });
    this.fetchStatesR();
}
fetchStatesR(estadoDelUsuario = true) {
  if (this.user) {
    if(estadoDelUsuario){
      this.stateService
      .getStatesByPaisIdCallHttp(Number(this.user.ciudades.estado.idPaisF))
      .subscribe((resp: EstadoGetResponse | any) => {
        console.log('resp ', resp);
        this.estadosR = resp;
      });
      this.fetchCitiesR();
    }else{
      this.stateService
      .getStatesByPaisIdCallHttp(Number(this.countriesService.selectedCountry))
      .subscribe((resp: EstadoGetResponse | any) => {
        console.log('resp ', resp);
        this.estadosR = resp;
      });
    }
  } else {
    this.estadosR.estados = [];
  }
}
  fetchCitiesR(mostrarSegunLaCiudadDelUsuario = true): void {
    if (this.user) {
        if(mostrarSegunLaCiudadDelUsuario){
          this.citiesService
          .getCitiesByStateIdCallHttp(Number(this.user.idCiudadF))
          .subscribe((resp: CiudadesGetResponse | any) => {
            console.log('resp ', resp);
            this.ciudadesR = resp;
          });
        }else{
          this.citiesService
          .getCitiesByStateIdCallHttp(Number(this.stateService.selectedState))
          .subscribe((resp: CiudadesGetResponse | any) => {
            console.log('resp ', resp);
            this.ciudadesR = resp;
          });
        }
      } else {
        this.ciudadesR.ciudades = [];
      }
    /*
    if (this.user) {
      this.ciudadesR = this.userService.cargarStorage('cities');
      this.selectedCity = this.user.idCiudadF.toString();
    } else {
      this.ciudadesR.ciudades = [];
    }
    */
  }
  fetchGenders() {
    if (this.user) {
      this.gendersR = this.userService.cargarStorage('genders');
      this.selectedGender = this.user.idGenderF.toString();
    } else {
      this.gendersR.genders = [];
    }
  }
  inicializarFormulario() {
    this.forma = this.fb.group(
      {
        namesU: [this.user.namesU, [Validators.required]],
        lastNamesU: [this.user.lastNamesU, [Validators.required]],
        emailU: {value: this.user.emailU, disabled: true},
        password: [''],
        password2: [''],
        identificationU: [
          this.user.identificationU.split("-")[1],
          [
            Validators.required
            // Validators.pattern('[V-]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$'),
          ],
        ],
        dateOfBirth: [this.fechaFormateada, [Validators.required]],
        directionU: [this.user.directionU, [Validators.required]],
        tlf1: [
          this.user.tlf1,
          [
            Validators.required,
            Validators.pattern('[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$'),
          ],
        ],
        tlf2: [this.user.tlf2],
        letraIdentificacion: [this.user.identificationU.split("-")[0], [Validators.required]],
        idCiudadF: [this.user.idCiudadF, [Validators.required]],
        idGenderF: [this.user.idGenderF, [Validators.required]],
        idEstado        : [this.user.ciudades.idEstadoF, [Validators.required]],
        idPais          : [this.user.ciudades.estado.idPaisF, [Validators.required]],
      },
      {
        validators: this.sonIguales('password', 'password2'),
      }
    );
  }
  editUser(): void {
    console.log('this.forma ', this.forma);
    // Resaltar errores si los hay
    this.forma.markAllAsTouched();
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    // Es valido. Crear el objeto.
    const usuarioAEditar: EditUserNoImage = {
      idUser: this.user.idUser,
      emailU: this.user.emailU,
      password: this.forma.value.password,
      password2: this.forma.value.password2,
      namesU: this.forma.value.namesU,
      lastNamesU: this.forma.value.lastNamesU,
      identificationU: this.forma.value.letraIdentificacion + '-' + this.forma.value.identificationU,
      dateOfBirth: this.forma.value.dateOfBirth,
      directionU: this.forma.value.directionU,
      tlf1: this.forma.value.tlf1,
      tlf2: this.forma.value.tlf2,
      idCiudadF: this.forma.value.idCiudadF,
      idGenderF: this.forma.value.idGenderF,
      token: this.token,
    };
    // For debugging.
    console.log('usuarioAEditar ', usuarioAEditar);
    Swal.fire({
      title: '¿Estás seguro/a?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        // Editar mediante el servicio.
        this.userService.editUser(usuarioAEditar).subscribe(
          (resp : EditUserByIdResponse) => {
            console.log("this.selectedFile ", this.selectedFile);
            console.log("resp.user.img ", resp.user.img);
        // Se subio una img nueva?
        if (this.selectedFile) {
          // No habia una img antes
          if(!resp.user.img || resp.user.img.length === 0){
            this.addImg(
              'users',
              resp.user.idUser,
              this.selectedFile
            );
            // Habia una img antes, editarla.
          }else if(resp.user.img.length > 0 && this.idImage){
            this.editImg(this.idImage, this.selectedFile);
          }
          
        }
          console.log('resp ', resp);
          this.router.navigate(['/home']);
        });
      }
    });
  }
  iniciarlizarPaisEstadoCiudad(){
    this.inicializarPaisVacio();
    this.inicializarEstadoVacio();
    this.inicializarCiudadVacia();
  }
  /*
  Doc: iniciarlizarObjLoginRSinDatos. Inicializa el obj user para que en caso de que el usuario no se haya logueado no de error.
  */
  iniciarlizarObjLoginRSinDatos(): void {
    this.user = {
      idUser: 0,
      emailU: '',
      password: '',
      img: null,
      namesU: '',
      lastNamesU: '',
      identificationU: '',
      dateOfBirth: new Date(),
      isActive: false,
      directionU: '',
      tlf1: '',
      tlf2: '',
      isSuperAdministrator: false,
      createdAt: new Date(),
      updatedAt: new Date(),
      idCiudadF: 0,
      idGenderF: 0,
      ciudades: {
        idCiudad: 0,
        nameCiudad: '',
        isVisible: true,
        createdAt: new Date(),
        updatedAt: new Date(),
        idEstadoF: 0,
        estado: {
          idEstado: 0,
          nombreEstado: '',
          isVisible: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          idPaisF: 0,
          paises: {
            idPais: 0,
            nameP: '',
            isVisible: true,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
        },
      },
      sexos: {
        idGender: 0,
        nameGender: '',
        isVisible: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    };
  }

  /*
  Doc: Dice si los passwords son iguales.
  Parametros: password1, password2.
  Regresa un booleano o un null.
  */
  sonIguales(campo1: string, campo2: string) {
    return (group: FormGroup) => {
      const pass1 = group.controls[campo1].value;
      const pass2 = group.controls[campo2].value;
      if (pass1 === pass2) {
        return null;
      }
      return {
        sonIguales: true,
      };
    };
  }
  // Choose city using select dropdown
  changeCity(e) {
    // console.log(e.target.value);
    this.idCiudadF.setValue(e.target.value, {
      onlySelf: true,
    });
  }
  onChangeState(newValue) {
    // this.cityService.selectedCity = newValue;
    this.stateService.selectedState = newValue.target.value;
    console.log('onChangeCountry newValue.target.value ', newValue.target.value);
    this.idEstado.setValue(newValue.target.value, {
      onlySelf: true
    });
    // Actualizar storage de la ciudad seleccionada.
    this.stateService.saveStateSelected(this.stateService.selectedState);
    this.fetchCitiesR(false);
  }
  onChangeCountry(newValue) {
    this.inicializarEstadoVacio();
    this.inicializarCiudadVacia();
    console.log('onChangeCountry newValue.target.value ', newValue.target.value);
    this.idPais.setValue(newValue.target.value, {
      onlySelf: true
    })
    // this.cityService.selectedCity = newValue;
    this.countriesService.selectedCountry = newValue.target.value;
    // Actualizar storage de la ciudad seleccionada.
    this.countriesService.saveCountrySelected(
      this.countriesService.selectedCountry
    );
    this.fetchStatesR(false);
    this.inicializarCiudadVacia();
  }
  changeGender(e) {
    // console.log(e.target.value);
    this.idGenderF.setValue(e.target.value, {
      onlySelf: true,
    });
  }
  inicializarCiudadVacia(){
    this.ciudadesR = {
      ok: false,
      ciudades: [{
        idCiudad:   0,
        nameCiudad: '',
        isVisible:  true,
        createdAt:  new Date(),
        updatedAt:  new Date(),
        idEstadoF: 0,
        estado: {
          idEstado: 0,
          nombreEstado: '',
          isVisible: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          idPaisF: 0,
          paises: {
            idPais: 0,
            nameP: '',
            isVisible: true,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
        },
      }]
    }
  }
  inicializarGenderVacia(){
    this.gendersR = {
      ok: false,
      genders: [{
        idGender:   0,
        nameGender: '',
        isVisible:  true,
        createdAt:  new Date(),
        updatedAt:  new Date(),
      }]
    }
  }
  inicializarEstadoVacio() {
    this.estadosR = {
      ok: true,
      cantidadEstados: 0,
      estados: [
        {
          idEstado: 0,
          nombreEstado: '',
          isVisible: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          idPaisF: 0,
          paises: {
            idPais: 0,
            nameP: "",
            isVisible: true,
            createdAt: new Date(),
            updatedAt: new Date(),
        }
        },
      ],
    };
  }
  inicializarPaisVacio() {
    this.paisesR = {
      ok: true,
      paises: [
          {
              idPais: 0,
              nameP: "",
              isVisible: true,
              createdAt: new Date(),
              updatedAt: new Date(),
          }
      ]
  };
  }
  onFileSelected(event: any) {
    if (event.target.files) {
      this.selectedFile = <File>event.target.files[0];
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (evento: any) => {
        this.previsualizarImg = evento.target.result;
      };
    }
  }
  addImg(tipo: string, id: number, imagen: File) {
    let token: string = this.userService.cargarStorage('token');
    this.imageService.addImg(tipo, id, imagen, token).subscribe(()=>{
      // TODO - Actualizar del local storage el campo img del usuario.
    });
  }
  editImg(idImg: number, imagen: File) {
    this.imageService.editImg(idImg, imagen).subscribe((resp: any)=>{
      // Actualizar el campo img del local stora user.
      let user: User = this.userService.cargarStorage("user");
      user.img = resp.imagen.nameImage;
      this.userService.usuario.img = resp.imagen.nameImage;
      console.log("user IMPORTA A GUARDAR", user);
      this.userService.guardarStorage("user", user);
    });
  }
    /*
  Doc: Validacion general que te devuelve un boolean para ver si un campo tiene o no algo.
  */
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }
  /* Getters */
  get idCiudadF() {
    return this.forma.get('idCiudadF');
  }
  get idEstado() {
    return this.forma.get('idEstado');
  }
  get idPais() {
    return this.forma.get('idPais');
  }
  get idGenderF() {
    return this.forma.get('idGenderF');
  }
  /* Gets para saber si un campo es valido */
  get nombreNoValido(): boolean {
    return this.forma.get('namesU').invalid && this.forma.get('namesU').touched;
  }
  get apellidoNoValido(): boolean {
    return (
      this.forma.get('lastNamesU').invalid &&
      this.forma.get('lastNamesU').touched
    );
  }
  get fechaNoValido(): boolean {
    return (
      this.forma.get('dateOfBirth').invalid &&
      this.forma.get('dateOfBirth').touched
    );
  }
  get correoNoValido(): boolean {
    return this.forma.get('emailU').invalid && this.forma.get('emailU').touched;
  }
  get tlf1NoValido(): boolean {
    return this.forma.get('tlf1').invalid && this.forma.get('tlf1').touched;
  }
  get identificacionNoValido(): boolean {
    return (
      this.forma.get('identificationU').invalid &&
      this.forma.get('identificationU').touched
    );
  }
  get ciudadNoValido(): boolean {
    return (
      this.forma.get('idCiudadF').invalid && this.forma.get('idCiudadF').touched
    );
  }
  get direccionNoValido(): boolean {
    return (
      this.forma.get('directionU').invalid &&
      this.forma.get('directionU').touched
    );
  }
  get generoNoValido(): boolean {
    return (
      this.forma.get('idGenderF').invalid && this.forma.get('idGenderF').touched
    );
  }
  /*get pass1NoValido(): boolean {
    return (
      this.forma.get('password').invalid && this.forma.get('password').touched
    );
  }*/
  get passwordMuyCorta(): boolean {
    return this.forma.get('password').errors.minlength;
  }
  get pass2NoValido(): boolean {
    const pass1 = this.forma.get('password').value;
    const pass2 = this.forma.get('password2').value;
    // console.log('this.forma.get.pass2.touched ', this.forma.get('password2').touched);
    if (pass1.length === 0) return false; // No se escribio nada en el campo password1
    // if (!this.forma.get('password2').touched) return false;
    return pass1 === pass2 && this.forma.get('password2').touched
      ? false
      : true;
  }
  get pass1NoValido(): boolean {
    const pass1 = this.forma.get('password').value;
    const pass2 = this.forma.get('password2').value;
    // console.log('this.forma.get.pass2.touched ', this.forma.get('password2').touched);
    if (pass2.length === 0) return false; // No se escribio nada en el campo password1
    // if (!this.forma.get('password2').touched) return false;
    return pass1 === pass2 && this.forma.get('password2').touched
      ? false
      : true;
  }
}
