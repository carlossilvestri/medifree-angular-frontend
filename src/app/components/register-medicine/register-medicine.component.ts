import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  Categoria,
  Medicine,
  MedicineDatosEditRequeridos,
  MedicineDatosRequeridos,
  MedicineGetPublicResponse,
} from 'src/app/interfaces/interfaces';
import { CategoryMedService } from 'src/app/services/category-med/category-med.service';
import { ImageService } from 'src/app/services/image/image.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-medicine',
  templateUrl: './register-medicine.component.html',
  styleUrls: ['./register-medicine.component.scss'],
})
export class RegisterMedicineComponent implements OnInit {
  categorias: Categoria[];
  forma: FormGroup;
  token: string = '';
  idMedicineParam: string = '';
  editMode: boolean = false;
  medicineEdit: Medicine;
  titulo: string = 'Agregar Medicamento';
  tituloBoton: string = 'Crear';
  idRadio: string = '';
  selectedFile: File = null;
  previsualizarImg: string = '';
  constructor(
    private userService: UserService,
    private medicineService: MedicinesService,
    private categoryMedService: CategoryMedService,
    private validationService: ValidationsService,
    private fb: FormBuilder,
    public router: Router,
    private activatedRoute: ActivatedRoute,
    public imageService: ImageService
  ) {}

  ngOnInit(): void {
    this.iniciarlizarInit();
  }
  iniciarlizarInit() {
    // Revisar si hay un parametro para editar un medicamento.
    this.activatedRoute.params.subscribe((params: any) => {
      // tslint:disable-next-line: no-string-literal
      this.idMedicineParam = params['id'];
      if (this.idMedicineParam) {
        this.editMode = true;
        this.titulo = 'Editar Medicamento';
        this.tituloBoton = 'Editar';
        this.getMedicineDetails(Number(this.idMedicineParam));
        this.imageService
          .getImagesCallHttp('medicines', Number(this.idMedicineParam))
          .subscribe();
        console.log('this.idMedicineParam ', this.idMedicineParam);
      } else {
        this.editMode = false;
        this.titulo = 'Agregar Medicamento';
        this.tituloBoton = 'Agregar';
      }
    });
    this.iniciarlizarategorias();
    this.inicializarFormulario();
  }
  getMedicineDetails(idMedicine: number) {
    // Buscar el medicamento por su id del array de medicines-of-user
    const medicineRespOfUser: MedicineGetPublicResponse =
      this.medicineService.getMedicinesOfUserLocalStorage();
    for (let i = 0; i < medicineRespOfUser.cantidadMedicamentos; i++) {
      if (medicineRespOfUser.medicines[i].idMedicine === idMedicine) {
        this.medicineEdit = medicineRespOfUser.medicines[i];
        console.log('Encontrada! ', this.medicineEdit);
        return;
      }
    }
  }
  iniciarlizarategorias() {
    this.token = this.userService.cargarStorage('token');
    // LLenar las categorias de los medicamentos.
    const categories: Categoria[] =
      this.categoryMedService.getCategoriesLocalStorage();
    /*
    // For debugging.
    console.log('categories ', categories);
    */
    if (!categories) {
      this.categorias =
        this.categoryMedService.iniciarlizarObjCategoriasSinDatos();
    } else {
      this.categorias = categories;
    }
  }
  inicializarFormulario() {
    this.forma = this.fb.group({
      nameM: [
        this.editMode ? this.medicineEdit.nameM : '',
        [Validators.required],
      ],
      descriptionM: [
        this.editMode ? this.medicineEdit.descriptionM : '',
        [Validators.required],
      ],
      inventaryM: [
        this.editMode ? this.medicineEdit.inventaryM : '',
        [Validators.required],
      ],
      idCategoriaF: [
        this.editMode ? this.medicineEdit.idCategoriaF : '',
        [Validators.required],
      ],
    });
  }
  onChangeRandioButton(event: any) {
    this.idRadio = event.target.value;
  }
  onSendInfo(): void {
    // Resaltar errores si los hay
    this.forma.markAllAsTouched();

    // For debugging.
    /*
    console.log('this.forma ', this.forma);
    console.log('selectedFile ', this.selectedFile);
    return;
    */
    // Revisar si el formulario es valido.
    if (this.forma.invalid) {
      return;
    }
    /*
    // For debugging.
    console.log('medicamentoACrear ', medicamentoACrear);
    console.log('this.forma ', this.forma);
    */
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        if (this.editMode) {
          // Es valido. Crear el objeto.
          const medicamentoAEditar: MedicineDatosEditRequeridos = {
            nameM: this.forma.value.nameM,
            descriptionM: this.forma.value.descriptionM,
            inventaryM: this.forma.value.inventaryM,
            idCategoriaF: this.forma.value.idCategoriaF,
            token: this.token,
            idMedicine: Number(this.idMedicineParam),
          };
          // El usuario cambio la img principal?
          if (this.idRadio.length > 0) {
            this.editMainImage(Number(this.idRadio));
          }
          // Editar medicina los campos, separada de las img.
          this.editMedicineOfUser(medicamentoAEditar);
        }
        if (!this.editMode) {
          // Es valido. Crear el objeto.
          const medicamentoACrear: MedicineDatosRequeridos = {
            nameM: this.forma.value.nameM,
            descriptionM: this.forma.value.descriptionM,
            inventaryM: this.forma.value.inventaryM,
            idCategoriaF: this.forma.value.idCategoriaF,
            idUsuarioF: this.forma.value.idUsuarioF,
            token: this.token,
          };
          this.crearMedicamento(medicamentoACrear);
        }
      }
    });
  }
  campoNoEsValido(campo: string): boolean {
    return this.validationService.campoNoEsValido(campo, this.forma);
  }
  onFileSelected(event: any) {
    if (event.target.files) {
      this.selectedFile = <File>event.target.files[0];
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (evento: any) => {
        this.previsualizarImg = evento.target.result;
      };
    }
  }
  // Crear medicamento.
  crearMedicamento(medicamentoACrear: MedicineDatosRequeridos) {
    this.medicineService.createMedicine(medicamentoACrear).subscribe((resp) => {
      console.log('resp ', resp);
      // Se agrego una image?
      // Se subio una img nueva?
      if (this.selectedFile) {
        this.addImg(
          'medicines',
          resp.medicine.idMedicine,
          this.selectedFile
        );
      }else{
        this.router.navigate(['/home/mis-medicamentos']);
      }
    });
  }
  // Edit medicamento.
  editMedicineOfUser(medicamentoAEditar: MedicineDatosEditRequeridos) {
    this.medicineService
      .editMedicineOfUser(medicamentoAEditar)
      .subscribe((resp) => {
        // Se subio una img nueva?
        if (this.selectedFile) {
          this.addImg(
            'medicines',
            Number(this.idMedicineParam),
            this.selectedFile
          );
        }
        console.log('resp ', resp);
        this.router.navigate(['/home/mis-medicamentos']);
      });
  }
  /**
   * Eliminar una imagen de la base de datos.
   * Pregunta si el usuario está seguro/a de eliminar la imagen y después se comunica manda a llamar el servicio con la funcion de eliminar la imagen.
   */
  deleteAnImage(idMedicamento: number): void {
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        this.imageService.deleteAnImage(idMedicamento).subscribe(() => {
          this.router.navigate(['/home/mis-medicamentos']);
          Swal.fire('Correcto', 'La imagen fue borrada con éxito.', 'success');
        });
      }
    });
  }
  editMainImage(idImage: number) {
    this.imageService.editMainImgField(idImage, true).subscribe();
  }
  addImg(tipo: string, id: number, imagen: File) {
    let token: string = this.userService.cargarStorage('token');
    console.log('token ', token);
    this.imageService.addImg(tipo, id, imagen, token).subscribe(()=>{
      this.router.navigate(['/home/mis-medicamentos']);
    });
  }
}
