import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudDonacionComponent } from './detalle-solicitud-donacion.component';

describe('DetalleSolicitudDonacionComponent', () => {
  let component: DetalleSolicitudDonacionComponent;
  let fixture: ComponentFixture<DetalleSolicitudDonacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudDonacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudDonacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
