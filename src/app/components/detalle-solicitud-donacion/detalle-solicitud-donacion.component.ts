import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  DonanteSeleccionadoGetResponse,
  PeticionDonacion,
  PeticionDonacionGetResponse,
  User,
} from 'src/app/interfaces/interfaces';
import { DonanteSeleccionadoService } from 'src/app/services/donante-seleccionado/donante-seleccionado.service';
import { PeticionDonacionService } from 'src/app/services/peticion-donacion/peticion-donacion.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-solicitud-donacion',
  templateUrl: './detalle-solicitud-donacion.component.html',
  styleUrls: ['./detalle-solicitud-donacion.component.scss'],
})
export class DetalleSolicitudDonacionComponent implements OnInit {
  idPeticionDonacionParam: number = 0;
  peticionDonacionGetResponse: PeticionDonacionGetResponse;
  peticionDonacion: PeticionDonacion;
  medicineCategory: string = '';
  usuarioLogueadoEsElCreador: boolean = false;
  user: User;
  isInDonanteSPage: boolean = false;
  idDonanteSParam: number = 0;
  idDSIsRepeated: boolean = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private peticionDonacionService: PeticionDonacionService,
    public donanteSeleccionado: DonanteSeleccionadoService,
    public userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.iniciarInit();
  }
  iniciarInit() {
    this.fetchUser();
    // Se esta en la pag de recibir o de donar?
    let stringArray: string[] = this.router.url.split('/');
    for(let i = 0; i < stringArray.length; i++){
      if(stringArray[i] === 'detalle-solicitud-medicamento'){
        // this.usuarioLogueadoEsElCreador = 
      }
      if(stringArray[i] === 'solicitud-donacion'){

      }
      if(stringArray[i] === 's-d'){
        this.isInDonanteSPage = true;
      }
    }
    this.cargarPeticionDonacion();
    this.usuarioLogueadoEsElCreador = this.getConocerSiElUsuarioLogueadoEsElCreador();
    console.log('stringArray ', stringArray);
    /*switch (this.router.url) {
      case '/home':
        this.isInDonanteSPage = true;
        break;
      default:
        this.isInDonanteSPage = false;
        break;
    }*/
  }
  fetchUser(){
    this.user = this.userService.cargarStorage('user');
  }
  cargarPeticionDonacion() {
    this.activatedRoute.params.subscribe((params: any) => {
      // tslint:disable-next-line: no-string-literal
      this.idPeticionDonacionParam = Number(params['id']);
      this.idDonanteSParam = Number(params['idDS']);
    });
    console.log('this.idPeticionDonacionParam ', this.idPeticionDonacionParam);
    this.peticionDonacionGetResponse =
      this.peticionDonacionService.getPeticionDonacionLocalStorage();
      console.log('this.peticionDonacionGetResponse ', this.peticionDonacionGetResponse);
    if (
      !this.peticionDonacionGetResponse ||
      this.peticionDonacionGetResponse == undefined
    ) {
      this.router.navigateByUrl('/home');
    }
    console.log('this.isInDonanteSPage ', this.isInDonanteSPage)
    // Cargar los datos de la peticion de la donacion seleccionada, para mostrar los datos.
    if(!this.isInDonanteSPage){
      for (
        let i: number = 0;
        i < this.peticionDonacionGetResponse.peticionDonacion.length;
        i++
      ) {
        if (
          this.peticionDonacionGetResponse.peticionDonacion[i].idPDonacion ===
          this.idPeticionDonacionParam
        ) {
          this.peticionDonacion =
            this.peticionDonacionGetResponse.peticionDonacion[i];
          return;
        }
      }
      this.router.navigateByUrl('/home');
    }else{
      let donantesSGetResponse: DonanteSeleccionadoGetResponse = this.userService.cargarStorage('donantes-s-creador');
      console.log('donantesSGetResponse ', donantesSGetResponse);
      for (
        let i: number = 0;
        i < donantesSGetResponse.donanteS.length;
        i++
      ) {
        if (
          donantesSGetResponse.donanteS[i].idPDonacionF ===
          this.idPeticionDonacionParam
        ) {
          this.peticionDonacion =
            donantesSGetResponse.donanteS[i].peticionDonacion;
          console.log('En else this.peticionDonacion ', this.peticionDonacion);

          return;
        }
      }
      // this.router.navigateByUrl('/home');
    }
  }
  getConocerSiElUsuarioLogueadoEsElCreador(): boolean {
    console.log('this.user en getConocerSiElUsuarioLogueadoEsElCreador ', this.user);
    console.log('this.peticionDonacion en getConocerSiElUsuarioLogueadoEsElCreador ', this.peticionDonacion);
    if (!this.user || !this.peticionDonacion) {
      return false;
    }
    // Hay Usuario.
    // Buscar en el array si el idUsuarioF === idUser (Si el usuario logueado es el creador).
    /* for (
      let i: number = 0;
      i < this.peticionDonacionGetResponse.peticionDonacion.length;
      i++
    ) {
      if (
        this.peticionDonacionGetResponse.peticionDonacion[i].medicamento.creador.idUser ===
        this.user.idUser
      ) {
        return true;
      }
    } */
    console.log('this.peticionDonacion ', this.peticionDonacion);
    // Comprobar si en el obj existen las propiedades.
    if(this.peticionDonacion.medicamento){
      if(this.peticionDonacion.medicamento.creador){
        if(this.peticionDonacion.medicamento.creador.idUser === this.user.idUser){
          return true;
        }
      }
    }
    return false;
  }
  crearDonanteSeleccionado(){
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        this.elegirParaDarDonacion();
      }
    });
  }
  borrarDonanteSeleccionadoPreg(){
    Swal.fire({
      title: '¿Estás seguro?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
    }).then((result) => {
      if (result.isConfirmed) {
        this.borrarDonanteSeleccionado();
      }
    });
  }
  elegirParaDarDonacion(){
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    const token: string = this.userService.cargarStorage('token');
    console.log('token ', token);
    console.log('this.idPeticionDonacionParam ', this.idPeticionDonacionParam);
    this.donanteSeleccionado
      .createDonanteSeleccionado(token, this.idPeticionDonacionParam)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
          Swal.fire(
            'Correcto',
            'El Donante Seleccionado fue creado con éxito.',
            'success'
          );
          this.router.navigateByUrl('/home/solicitud-donacion');
        },
        (err) => {
          console.log('err ', err);
        }
      );
  }
  borrarDonanteSeleccionado(){
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    const token: string = this.userService.cargarStorage('token');
    console.log('token ', token);
    console.log('this.idDonanteSParam ', this.idDonanteSParam);
    this.donanteSeleccionado
      .deleteDonanteS(this.idDonanteSParam, token)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
            // Notificacion de que el medicamento se borro con exito.
            Swal.fire('Correcto', 'El Donante Seleccionado fue borrado con éxito.', 'success');
            this.router.navigateByUrl('/home/donantes-seleccionados');
        },
        (err) => {
          console.log('err ', err);
        }
      );
  }
}
