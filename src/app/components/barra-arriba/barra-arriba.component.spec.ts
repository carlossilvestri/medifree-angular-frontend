import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarraArribaComponent } from './barra-arriba.component';

describe('BarraArribaComponent', () => {
  let component: BarraArribaComponent;
  let fixture: ComponentFixture<BarraArribaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarraArribaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarraArribaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
