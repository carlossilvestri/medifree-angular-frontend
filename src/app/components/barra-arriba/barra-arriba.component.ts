import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {
  CiudadesGetResponse,
  EstadoGetResponse,
  Paises,
  User,
  UserCorto,
} from 'src/app/interfaces/interfaces';
import { CitiesService } from 'src/app/services/cities/cities.service';
import { EstadoService } from 'src/app/services/estado/estado.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { UserService } from 'src/app/services/user.service';
import { PaisService } from '../../services/pais/pais.service';
import { PaisesGetResponse } from '../../interfaces/interfaces';

@Component({
  selector: 'app-barra-arriba',
  templateUrl: './barra-arriba.component.html',
  styleUrls: ['./barra-arriba.component.scss'],
})
export class BarraArribaComponent implements OnInit {
  ciudadesR: CiudadesGetResponse;
  estadosR: EstadoGetResponse;
  paisesR: PaisesGetResponse;
  user: User;
  esconder: boolean = true;
  keyword: string;
  // @Output() changeCity: EventEmitter<number> = new EventEmitter();
  constructor(
    public cityService: CitiesService,
    public stateService: EstadoService,
    public countriesService: PaisService,
    private userService: UserService,
    public medicinesService: MedicinesService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.iniciarlizarCampos();
  }
  /* Functions */
  iniciarlizarCampos() {
    // this.ciudadesR = this.cityService.getCitiesLocalStorage();
    this.user = this.userService.cargarStorage('user');
    if (this.user) {
      this.countriesService.selectedCountry =
        this.user.ciudades.estado.idPaisF.toString();
      this.countriesService.saveCountrySelected(
        this.countriesService.selectedCountry
      );
      this.stateService.selectedState = this.user.ciudades.idEstadoF.toString();
      this.stateService.saveStateSelected(this.stateService.selectedState);
      this.cityService.selectedCity = this.user.idCiudadF.toString();
      this.cityService.saveCitySelected(this.cityService.selectedCity);
    }
      this.fetchCountriesR();
  }
  toggleEsconder() {
    if (this.esconder) {
      this.esconder = false;
    } else {
      this.esconder = true;
    }
  }
  fetchCitiesR(): void {
        // El usuario no esta logueado. Pero se selecciono un pais?
        if(this.stateService.selectedState){
          this.cityService
          .getCitiesByStateIdCallHttp(Number(this.stateService.selectedState))
          .subscribe((resp: CiudadesGetResponse | any) => {
            this.ciudadesR = resp;
          });
        }
  }
  fetchStatesR() {
    // El usuario esta logueado?
      if (this.user) {
        this.stateService
          .getStatesByPaisIdCallHttp(Number(this.user.ciudades.estado.idPaisF))
          .subscribe((resp: EstadoGetResponse | any) => {
            console.log('resp ', resp);
            this.estadosR = resp;
          });
      } else {
        // El usuario no esta logueado. Pero se selecciono un pais?
        if(this.countriesService.selectedCountry){
          this.stateService
          .getStatesByPaisIdCallHttp(
            Number(this.countriesService.selectedCountry)
          )
          .subscribe((resp: EstadoGetResponse | any) => {
            console.log('resp ', resp);
            this.estadosR = resp;
          });
        }
      }
      this.fetchCitiesR();
  }
  fetchCountriesR() {
    // Angular necesita los datos inmetiadamente, pero no hay ciudades en el local storage asi que miestras carga la solicitud con las ciudades se coloca un array vacio.
    this.iniciarlizarPaisEstadoCiudad();
     // Llamar End Point para obtener los paises
    this.countriesService
      .getCountriesAvailablesCallHttp()
      .subscribe((resp: PaisesGetResponse | any) => {
        console.log('resp ', resp);
        this.paisesR = resp;
      });
      this.fetchStatesR();
  }
  onChangeCity(newValue) {
    console.log('newValue ', newValue);
    // this.cityService.selectedCity = newValue;
    this.cityService.selectedCity = newValue;
    // Actualizar storage de la ciudad seleccionada.
    this.cityService.saveCitySelected(this.cityService.selectedCity);
    this.medicinesService.page = 0;
    // Si el usuario seleccióno todas las ciudades:
    if (this.cityService.selectedCity === '') {
      this.medicinesService
        .getMedicinesByState(Number(this.stateService.selectedState), 0)
        .subscribe();
    } else {
      // El usuario seleccionó una ciudad, buscar medicamentos según la ciudad:
      this.medicinesService
        .getMedicinesByCity(Number(this.cityService.selectedCity), 0)
        .subscribe((resp) => {
          // console.log('resp medicines ', resp);
        });
    }
    // Esconder el poder escoger la ubicacion, para que el usuario tenga mas espacio al ver la pantalla.
    this.toggleEsconder();
    this.router.navigate(['/home']);
    // ... do other stuff here ...
  }
  onChangeState(newValue) {
    console.log('newValue ', newValue);
    // this.cityService.selectedCity = newValue;
    this.stateService.selectedState = newValue;
    // Actualizar storage de la ciudad seleccionada.
    this.cityService.saveCitySelected('');
    this.medicinesService.page = 0;
    console.log("this.stateService.selectedState ", this.stateService.selectedState);
    if (this.stateService.selectedState === '') {
      if (this.countriesService.selectedCountry === '') {
        this.medicinesService.getMedicinesAllCountries(0).subscribe();
      } else {
        this.medicinesService
          .getMedicinesByCountry(
            Number(this.countriesService.selectedCountry),
            0
          )
          .subscribe(()=>{
            this.inicializarCiudadVacia();
          });
      }
    } else {
      this.medicinesService
        .getMedicinesByState(Number(this.stateService.selectedState), 0)
        .subscribe((resp) => {
          // console.log('resp medicines ', resp);
        });
    }
    this.fetchCitiesR();
    // Esconder el poder escoger la ubicacion, para que el usuario tenga mas espacio al ver la pantalla.
    this.toggleEsconder();
    this.router.navigate(['/home']);
    // ... do other stuff here ...
  }
  onChangeCountry(newValue) {
    // Reiniciar campos:
    this.inicializarEstadoVacio();
    this.inicializarCiudadVacia();
    console.log('onChangeCountry newValue ', newValue);
    this.stateService.saveStateSelected('');
    this.cityService.saveCitySelected('');
    // Actualizar storage de; pais seleccionado.
    this.countriesService.saveCountrySelected(
      this.countriesService.selectedCountry
    );
    // Reiniciar la paginacion de los medicamentos a 0.
    this.medicinesService.page = 0;
    // Si el usuario seleccionó todos los países:
    if (this.countriesService.selectedCountry === '') {
      this.inicializarEstadoVacio();
      this.inicializarCiudadVacia();
      this.medicinesService.getMedicinesAllCountries(0).subscribe((resp) => {
        this.stateService.selectedState = '';
      });
    } else {
      // El usuario seleccióno un país, buscar medicamentos:
      this.medicinesService
        .getMedicinesByCountry(Number(this.countriesService.selectedCountry), 0)
        .subscribe((resp: PaisesGetResponse) => {
          this.stateService.selectedState = '';
          this.fetchStatesR();
        });
    }
    // Esconder el poder escoger la ubicacion, para que el usuario tenga mas espacio al ver la pantalla.
    this.toggleEsconder();
    this.router.navigate(['/home']);
    // ... do other stuff here ...
  }

  onSearchByKeyword() {
    if (this.keyword.trim().length > 0) {
      // Hay una ciudad seleccionada?
      if (this.cityService.selectedCity.length > 0) {
        this.medicinesService
          .getMedicinesByKeywordAndByCity(
            this.keyword,
            Number(this.cityService.selectedCity),
            0
          )
          .subscribe((resp) => {
            this.keyword = '';
          });
          // Hay una estado seleccionado?
      } else if (this.stateService.selectedState.length > 0) {
        this.searchMedicineByKeywordAndByStateId(
          this.keyword,
          Number(this.stateService.selectedState)
        );
        // Hay una país seleccionado?
      } else if (this.countriesService.selectedCountry.length > 0) {
        this.searchMedicineByKeywordAndByCountryId(
          this.keyword,
          Number(this.countriesService.selectedCountry)
        );
      } else {
        // Buscar en todos los paises.
        this.searchMedicineByKeyword(this.keyword);
      }
    }
  }
  inicializarCiudadVacia() {
    this.ciudadesR = {
      ok: false,
      ciudades: [
        {
          idCiudad: 0,
          nameCiudad: '',
          isVisible: true,
          createdAt: new Date(),
          updatedAt: new Date(),
          idEstadoF: 0,
          estado: {
            idEstado: 0,
            nombreEstado: '',
            isVisible: true,
            createdAt: new Date(),
            updatedAt: new Date(),
            idPaisF: 0,
            paises: {
              idPais: 0,
              nameP: '',
              isVisible: true,
              createdAt: new Date(),
              updatedAt: new Date(),
            },
          },
        },
      ],
    };
  }
  inicializarEstadoVacio() {
    this.estadosR = {
      ok: true,
      cantidadEstados: 0,
      estados: [],
    };
  }
  inicializarPaisVacio() {
    this.paisesR = {
      ok: true,
      paises: [],
    };
  }
  iniciarlizarPaisEstadoCiudad() {
    this.inicializarPaisVacio();
    this.inicializarEstadoVacio();
    this.inicializarCiudadVacia();
  }
  searchMedicineByKeyword(keywords: string) {
    if (keywords.length > 0) {
      this.medicinesService
        .getMedicinesByKeyword(keywords, 0)
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
          this.keyword = '';
        });
      this.router.navigate(['/home']);
      return;
    }
  }
  searchMedicineByKeywordAndByCountryId(keywords: string, idPaisF: number) {
    if (keywords.length > 0) {
      this.medicinesService
        .getMedicinesByKeywordAndByCountry(keywords, Number(idPaisF), 0)
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
          this.keyword = '';
        });
      this.router.navigate(['/home']);
      return;
    }
  }
  searchMedicineByKeywordAndByStateId(keywords: string, idEstado: number) {
    if (keywords.length > 0) {
      this.medicinesService
        .getMedicinesByKeywordAndByState(keywords, Number(idEstado), 0)
        .subscribe((resp) => {
          console.log('resp medicines ', resp);
          this.keyword = '';
        });
      this.router.navigate(['/home']);
      return;
    }
  }
}
