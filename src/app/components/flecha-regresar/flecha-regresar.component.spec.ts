import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlechaRegresarComponent } from './flecha-regresar.component';

describe('FlechaRegresarComponent', () => {
  let component: FlechaRegresarComponent;
  let fixture: ComponentFixture<FlechaRegresarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlechaRegresarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlechaRegresarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
