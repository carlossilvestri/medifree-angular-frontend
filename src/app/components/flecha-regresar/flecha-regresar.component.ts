import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-flecha-regresar',
  templateUrl: './flecha-regresar.component.html',
  styleUrls: ['./flecha-regresar.component.scss']
})
export class FlechaRegresarComponent implements OnInit {

  constructor(private location: Location) {}

  ngOnInit(): void {
  }

  back(): void {
    this.location.back()
  }

}
