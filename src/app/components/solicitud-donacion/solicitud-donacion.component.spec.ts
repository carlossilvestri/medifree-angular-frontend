import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudDonacionComponent } from './solicitud-donacion.component';

describe('SolicitudDonacionComponent', () => {
  let component: SolicitudDonacionComponent;
  let fixture: ComponentFixture<SolicitudDonacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudDonacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudDonacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
