import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PeticionDonacion } from 'src/app/interfaces/interfaces';
import { CategoryMedService } from 'src/app/services/category-med/category-med.service';
import { DonanteSeleccionadoService } from 'src/app/services/donante-seleccionado/donante-seleccionado.service';
import { MedicinesService } from 'src/app/services/medicines/medicines.service';
import { PeticionDonacionService } from 'src/app/services/peticion-donacion/peticion-donacion.service';
import { UserService } from 'src/app/services/user.service';
import { ValidationsService } from 'src/app/services/validations/validations.service';

@Component({
  selector: 'app-solicitud-donacion',
  templateUrl: './solicitud-donacion.component.html',
  styleUrls: ['./solicitud-donacion.component.scss'],
})
export class SolicitudDonacionComponent implements OnInit {
  titulo: string = '';
  token: string = '';
  solicitudRecibir: boolean = false;
  rutaLink: string = ''; 
  constructor(
    private userService: UserService,
    public peticionDonacion: PeticionDonacionService,
    public donanteSeleccionado: DonanteSeleccionadoService,
    private validationService: ValidationsService,
    private fb: FormBuilder,
    public router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.iniciarlizarInit();
  }
  iniciarlizarInit() {
    // Se esta en modo solicitudRecibir ?
    switch (this.router.url) {
      case '/home/solicitud-donacion':
        this.solicitudRecibir = false;
        this.titulo = 'Elegir para dar donación';
        this.fetchSolicitudDonantesDelDonador();
        break;
      case '/home/solicitud-donacion-recibir':
        this.solicitudRecibir = true;
        this.titulo = 'Mis solicitudes para recibir donaciones';
        this.fetchSolicitudDonantesDelSolicitante();
        break;
      default:
        this.solicitudRecibir = false;
        break;
    }
  }
  fetchSolicitudDonantesDelDonador() {
    this.token = this.userService.cargarStorage('token');
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    this.peticionDonacion
      .getPeticionesDonacionDelDonador(this.token, 0)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
        },
        (err) => {
          console.log('err ', err);
          this.peticionDonacion.huboUnError = true;
        }
      );
  }
  fetchSolicitudDonantesDelSolicitante() {
    this.token = this.userService.cargarStorage('token');
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    this.peticionDonacion
      .getPeticionesDonacionDelSolicitante(this.token, 0)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
        },
        (err) => {
          console.log('err ', err);
          this.peticionDonacion.huboUnError = true;
        }
      );
  }

  irA(idPDonacion: number){
    if(this.solicitudRecibir){
      this.router.navigateByUrl(`/home/solicitud-donacion/${idPDonacion}`);
    }else{
      this.router.navigateByUrl(`/home/solicitud-donacion-recibir/${idPDonacion}`); 
    }
  }
  elegirParaDarDonacion(idPDonacion: number){
    // LLenar las solicitudes o peticiones de donacion de los medicamentos.
    this.donanteSeleccionado
      .createDonanteSeleccionado(this.token, idPDonacion)
      .subscribe(
        (resp) => {
          console.log('resp ', resp);
        },
        (err) => {
          console.log('err ', err);
        }
      );
  }
}
