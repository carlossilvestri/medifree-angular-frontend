/* API Responses. */

export interface CreateAccountResponse {
    ok:    boolean;
    token: string;
    user:  UserCorto;
}
export interface LoginResponse {
    token: string;
    user:  User;
}
export interface ImgGetResponse {
    ok:       boolean;
    cantidad: number;
    imagenes: Imagen[];
}

export interface Imagen {
    idImage:        number;
    nameImage:      string;
    mainImage:      boolean;
    isVisible:      boolean;
    createdAt:      Date;
    updatedAt:      Date;
    idMedicamentoF: number;
    idUserF:        number;
}
export interface Imagen2 {
    image: string;
    thumbImage: string;
    main: boolean;
    id: number;
    order: number;
}
export interface ListaDeUsuariosResponse {
    ok:               boolean;
    cantidadUsuarios: number;
    users:            User[];
}
export interface PaisesGetResponse {
    ok:     boolean;
    paises: Paises[];
}
export interface GetUserByIdResponse {
    ok:   boolean;
    user: User;
}
export interface EditUserByIdResponse {
    ok:   boolean;
    msg:  string;
    user: User;
}
export interface ModificarPasswordResponse {
    ok:  boolean;
    msg: string;
}
export interface CiudadesGetResponse {
    ok:       boolean;
    ciudades: Ciudades[];
}
export interface EstadoGetResponse {
    ok:              boolean;
    cantidadEstados: number;
    estados:         Estado[];
}
export interface GenderGetResponse {
    ok:      boolean;
    genders: Sexos[];
}
export interface SecurityQResponse {
    ok: boolean;
    qr: Qr;
}
export interface MedicineResponse {
    ok:       boolean;
    medicine: Medicine;
}
export interface MedicineGetPublicResponse {
    ok:                   boolean;
    desde:                number;
    cantidadMedicamentos: number;
    medicines:            MedicineLong[];
}
export interface PeticionDonacionGetResponse {
    ok:               boolean;
    desde:            number;
    cantidadPD:       number;
    peticionDonacion: PeticionDonacion[];
}
export interface DonanteSeleccionadoGetResponse {
    ok:         boolean;
    desde:      number;
    cantidadDS: number;
    donanteS:   Donante[];
}
export interface DonanteSeleccionadoPostResponse {
    ok:       boolean;
    donanteS: Donante;
}
/* Campos para hacer las peticiones a la API */
export interface EditUserNoImage {
    idUser:               number;
    emailU:               string;
    password:             string | null;
    password2:            string | null;
    namesU:               string;
    lastNamesU:           string;
    identificationU:      string;
    dateOfBirth:          Date;
    directionU:           string;
    tlf1:                 string;
    tlf2:                 null | string;
    idCiudadF:            number;
    idGenderF:            number;
    token:                string;
}
export interface UserAlCrear {
    emailU:               string;
    password:             string;
    password2:             string;
    namesU:               string;
    lastNamesU:           string;
    identificationU:      string;
    dateOfBirth:          Date;
    directionU:           string;
    tlf1:                 string;
    tlf2:                 null | string;
    idCiudadF:            number;
    idGenderF:            number;
}
export interface LoginDatosRequeridos {
    emailU:               string;
    password:             string;
}
export interface SecurityQDatosRequeridosEdit{
    idQr:       number;
    q1:         string;
    q2:         string;
    r1:         string;
    r2:         string;
    token:      string;
}
export interface SecurityQDatosRequeridosCreate{
    q1:         string;
    q2:         string;
    r1:         string;
    r2:         string;
    idUsuarioF: number;
    token:      string;
}
export interface CategoryResponse {
    ok:         boolean;
    categorias: Categoria[];
}
export interface MedicineDatosRequeridos {
    nameM:        string;
    descriptionM: string;
    inventaryM:   string;
    idCategoriaF: string;
    idUsuarioF:   number;
    token:        string;
}
export interface MedicineDatosEditRequeridos {
    nameM:        string;
    descriptionM: string;
    inventaryM:   string;
    idCategoriaF: string;
    token:        string;
    idMedicine:   number;
}
export interface PeticionDonacionDatosReqCrear {
    token:       string;
    msjDonacion: string;
    idMedicineF: number;
}
/* ADMINISTRADOR */
export interface GetDatosAdm {
    ok:             boolean;
    cantUsers:      number;
    cantMedicines:  number;
    cantDonations:  number;
    cantCategories: number;
    cantCountries:  number;
    cantStates:     number;
    cantCities:     number;
}
export interface GetUser {
    ok:               boolean;
    cantidadUsuarios: number;
    users:            User[];
}
/* Entidades. */

export interface UserCorto {
    isSuperAdministrator: boolean;
    idUser:               number;
    emailU:               string;
    password:             string;
    namesU:               string;
    lastNamesU:           string;
    identificationU:      string;
    idCiudadF:            string;
    dateOfBirth:          Date;
    directionU:           string;
    idGenderF:            string;
    tlf1:                 string;
    tlf2:                 string;
    updatedAt:            Date;
    createdAt:            Date;
}
export interface Donante {
    idDonanteSeleccionado: number;
    createdAt:             Date;
    updatedAt:             Date;
    idPDonacionF:          number;
    peticionDonacion:      PeticionDonacion;
}

export interface PeticionDonacion {
    idPDonacion: number;
    msjDonacion: string;
    createdAt:   Date;
    updatedAt:   Date;
    idMedicineF: number;
    idUsuarioF:  number;
    medicamento: MedicineLong;
    solicitante: User;
    isSelected: boolean;
}
export interface Medicine {
    idMedicine:   number;
    nameM:        string;
    descriptionM: string;
    inventaryM:   number;
    idCategoriaF: string;
    idUsuarioF:   number;
    updatedAt:    Date;
    createdAt:    Date;
    isAvailable:  boolean;
    pictureM:     string | null;
}
export interface MedicineLong {
    idMedicine:   number;
    nameM:        string;
    descriptionM: string;
    inventaryM:   number;
    idCategoriaF: string;
    idUsuarioF:   number;
    updatedAt:    Date;
    createdAt:    Date;
    isAvailable:  boolean;
    categoria:    Categoria;
    ciudades:     Ciudades;
    creador:      User;
    isActive:     boolean;
    pictureM:     string | null;
}
export interface Categoria {
    idCategoria:   number;
    nameCategoria: string;
    isVisible:     boolean;
    createdAt:     Date;
    updatedAt:     Date;
}
export interface User {
    idUser:               number;
    emailU:               string;
    password:             string;
    img:                  null | string;
    isActive:             boolean;
    namesU:               string;
    lastNamesU:           string;
    identificationU:      string;
    dateOfBirth:          Date;
    directionU:           string;
    tlf1:                 string;
    tlf2:                 null | string;
    isSuperAdministrator: boolean | null;
    createdAt:            Date;
    updatedAt:            Date;
    idCiudadF:            number;
    idGenderF:            number;
    ciudades:             Ciudades;
    sexos:                Sexos;
}
export interface Ciudades {
    idCiudad:   number;
    nameCiudad: string;
    isVisible:  boolean;
    createdAt:  Date;
    updatedAt:  Date;
    idEstadoF:    number;
    estado:     Estado;
}
export interface Estado {
    idEstado:     number;
    nombreEstado: string;
    isVisible:    boolean;
    createdAt:    Date;
    updatedAt:    Date;
    idPaisF:      number;
    paises:       Paises;
}
export interface Paises {
    idPais:    number;
    isVisible: boolean;
    nameP:     string;
    createdAt: Date;
    updatedAt: Date;
}
export interface Sexos {
    idGender:   number;
    nameGender: string;
    isVisible:  boolean;
    createdAt:  Date;
    updatedAt:  Date;
}
export interface Qr {
    idQr:       number;
    q1:         string;
    q2:         string;
    r1:         string;
    r2:         string;
    idUsuarioF: string;
}
